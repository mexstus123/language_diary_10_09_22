package uk.ac.aber.dcs.cs31620.language_diary_mik46

import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.lifecycle.viewmodel.compose.viewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.theme.LanguageDiaryTheme

class SpellingGameTests {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    private lateinit var database: UserWordsViewModel
    private lateinit var testListSize4 : List<UserWord>
    private lateinit var testListSize12 : List<UserWord>

    @Before
    fun listSetUp(){
        testListSize4 = List<UserWord>(4){ index -> UserWord("dog${index}", "perro${index}") }
        testListSize12 = List<UserWord>(12){ index -> UserWord("dog${index}", "perro${index}") }

    }

    private fun getToSpellingTestOnEasy(){
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()
    }

    private fun goBackToTestsScreenAndConfirm(){
        composeTestRule.onNodeWithContentDescription("Go Back Arrow").performClick()
        composeTestRule.onNodeWithText("Spelling").assertIsDisplayed()
        composeTestRule.onNodeWithText("Pairing").assertIsDisplayed()
    }

    private fun goToNextQuestion(){
        composeTestRule.onNode(hasText("Next Question") and hasClickAction()).performClick()
        //sleep due to animations
        Thread.sleep(500)
    }

    @Test
    fun spellingBackNavigation() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize4)
                }
            }
        }
        getToSpellingTestOnEasy()
        goBackToTestsScreenAndConfirm()

    }

    @Test
    fun spellingBackNavigationFromQuestion2() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize4)
                }
            }
        }
        getToSpellingTestOnEasy()
        goToNextQuestion()
        goBackToTestsScreenAndConfirm()
    }

    @Test
    fun spellingBackNavigationFromQuestion3() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize4)
                }
            }
        }
        getToSpellingTestOnEasy()
        goToNextQuestion()
        goToNextQuestion()
        goBackToTestsScreenAndConfirm()
    }

    @Test
    fun spellingBackNavigationFromQuestion4() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize4)
                }
            }
        }
        getToSpellingTestOnEasy()
        goToNextQuestion()
        goToNextQuestion()
        goToNextQuestion()
        goBackToTestsScreenAndConfirm()
    }

    @Test
    fun spellingBackNavigationFromEndScreen() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize4)
                }
            }
        }
        getToSpellingTestOnEasy()
        goToNextQuestion()
        goToNextQuestion()
        goToNextQuestion()
        goToNextQuestion()
        composeTestRule.onNodeWithText("Questions Correct:").assertIsDisplayed()
        composeTestRule.onNodeWithText("Time Taken:").assertIsDisplayed()
        goBackToTestsScreenAndConfirm()
    }



    @Test
    fun spellingTextFieldMaxInputText() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize4)
                }
            }
        }
        getToSpellingTestOnEasy()

        for (i in 0..15){
            composeTestRule.onNode(hasText("Translate") and hasClickAction()).performTextInput("${i}")
        }
        composeTestRule.onNode(hasText("01234567891011") and hasClickAction()).assertIsDisplayed()
    }

    @Test
    fun spellingTextFieldSpaceInputText() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize4)
                }
            }
        }
        getToSpellingTestOnEasy()

        composeTestRule.onNode(hasText("Translate") and hasClickAction()).performTextInput(" ")
        composeTestRule.onNode(hasText("")).assertIsDisplayed()
    }

    @Test
    fun spellingTestBackNavigationForHard() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize12)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()

        for (test in 1..12) {
            for (question in 1..test) {
                goToNextQuestion()
            }
            goBackToTestsScreenAndConfirm()
            composeTestRule.onNodeWithText("Spelling").performClick()
        }
    }

    @Test
    fun spellingTestAllNavigationForMedium() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize12)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()

        for (test in 1..8) {
            for (question in 1..test) {
                goToNextQuestion()
            }
            goBackToTestsScreenAndConfirm()
            composeTestRule.onNodeWithText("Spelling").performClick()
        }
    }
}