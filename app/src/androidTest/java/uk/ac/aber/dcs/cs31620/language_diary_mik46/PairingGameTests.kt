package uk.ac.aber.dcs.cs31620.language_diary_mik46

import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.lifecycle.viewmodel.compose.viewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.theme.LanguageDiaryTheme

class PairingGameTests {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    private lateinit var database: UserWordsViewModel
    private lateinit var testList : List<UserWord>

    @Before
    fun listSetUp(){
        testList = List<UserWord>(5){ index -> UserWord("dog${index}", "ci${index}") }
    }

    private fun getToPairingTestOnEasy(){
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
    }

    private fun getToPairingTestOnHard(){
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
    }

    private fun goBackToTestsScreenAndConfirm(){
        composeTestRule.onNodeWithContentDescription("Go Back Arrow").performClick()
        composeTestRule.onNodeWithText("Spelling").assertIsDisplayed()
        composeTestRule.onNodeWithText("Pairing").assertIsDisplayed()
    }


    private fun guess(){
        composeTestRule.onNode(hasText("Guess") and hasClickAction()).performClick()
        //sleep due to animations
        Thread.sleep(500)
    }

    @Composable
    fun TestContentSetup(){
        database = viewModel()
        database.deleteAllFromDatabase()
        LanguageDiaryTheme{
            Surface(
                modifier = Modifier.fillMaxSize(),
                color = MaterialTheme.colorScheme.background
            ) {
                CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
            }
        }
    }

    @Test
    fun pairingBackNavigation() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToPairingTestOnEasy()
        composeTestRule.onNodeWithText("7").assertIsDisplayed()
        goBackToTestsScreenAndConfirm()
    }

    @Test
    fun pairingIncorrectGuessTillGameEnd() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToPairingTestOnEasy()
        for (i in 1..7){
            composeTestRule.onNodeWithText("dog1").performClick()
            composeTestRule.onNodeWithText("dog2").performClick()
            guess()
        }
        composeTestRule.onNodeWithText("Test Failed").assertIsDisplayed()
        goBackToTestsScreenAndConfirm()
    }

    @Test
    fun pairingCorrectGuessTillGameEnd() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToPairingTestOnEasy()
        for (i in 0..4){
            composeTestRule.onNodeWithText("dog${i}").performClick()
            composeTestRule.onNodeWithText("ci${i}").performClick()
            guess()
        }
        composeTestRule.onNodeWithText("Test Passed").assertIsDisplayed()
        goBackToTestsScreenAndConfirm()
    }

    @Test
    fun pairingAllCorrectGuessThenLoseGameVariations() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToPairingTestOnHard()
        for (test in 0..2) {
            for (questions in 0..test) {
                composeTestRule.onNodeWithText("dog${questions}").performClick()
                composeTestRule.onNodeWithText("ci${questions}").performClick()
                guess()
            }
            composeTestRule.onNodeWithText("dog4").performClick()
            composeTestRule.onNodeWithText("dog3").performClick()
            guess()
            composeTestRule.onNodeWithText("dog4").performClick()
            composeTestRule.onNodeWithText("dog3").performClick()
            guess()

            composeTestRule.onNodeWithText("Test Failed").assertIsDisplayed()
            goBackToTestsScreenAndConfirm()
            composeTestRule.onNodeWithText("Pairing").performClick()
        }
    }

    @Test
    fun pairingOneSelectedGuess() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToPairingTestOnEasy()

        composeTestRule.onNodeWithText("7").assertIsDisplayed()
        composeTestRule.onNodeWithText("dog4").performClick()
        guess()

        composeTestRule.onNodeWithText("7").assertIsDisplayed()

        guess()
        composeTestRule.onNodeWithText("7").assertIsDisplayed()

        composeTestRule.onNodeWithText("dog3").performClick()
        guess()

        composeTestRule.onNodeWithText("6").assertIsDisplayed()


    }

    @Test
    fun pairingNoneSelectedGuess() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToPairingTestOnEasy()

        composeTestRule.onNodeWithText("7").assertIsDisplayed()
        guess()

        composeTestRule.onNodeWithText("dog3").performClick()
        guess()

        composeTestRule.onNodeWithText("7").assertIsDisplayed()


        composeTestRule.onNodeWithText("dog4").performClick()
        guess()

        composeTestRule.onNodeWithText("6").assertIsDisplayed()

    }

    @Test
    fun pairingOnlyTwoSelectedLimit() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToPairingTestOnEasy()

        composeTestRule.onAllNodes(isNotEnabled()).assertCountEquals(0)

        composeTestRule.onNodeWithText("dog1").performClick()
        composeTestRule.onNodeWithText("ci1").performClick()
        composeTestRule.onNodeWithText("dog2").performClick()
        composeTestRule.onNodeWithText("ci2").performClick()
        composeTestRule.onNodeWithText("dog3").performClick()
        composeTestRule.onNodeWithText("ci3").performClick()

        guess()
        composeTestRule.onAllNodes(isNotEnabled()).assertCountEquals(2)

    }

    @Test
    fun pairingMultipleAttemptsButtonReEnabled() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToPairingTestOnEasy()

        composeTestRule.onAllNodes(isNotEnabled()).assertCountEquals(0)

        composeTestRule.onNodeWithText("dog1").performClick()
        composeTestRule.onNodeWithText("ci1").performClick()
        guess()

        composeTestRule.onAllNodes(isNotEnabled()).assertCountEquals(2)

        goBackToTestsScreenAndConfirm()
        composeTestRule.onNodeWithText("Pairing").performClick()

        composeTestRule.onAllNodes(isNotEnabled()).assertCountEquals(0)

        composeTestRule.onNodeWithText("dog1").performClick()
        composeTestRule.onNodeWithText("ci1").performClick()
        guess()
        composeTestRule.onNodeWithText("dog2").performClick()
        composeTestRule.onNodeWithText("ci2").performClick()
        guess()

        composeTestRule.onAllNodes(isNotEnabled()).assertCountEquals(4)

        goBackToTestsScreenAndConfirm()
        composeTestRule.onNodeWithText("Pairing").performClick()

        composeTestRule.onAllNodes(isNotEnabled()).assertCountEquals(0)

    }

}