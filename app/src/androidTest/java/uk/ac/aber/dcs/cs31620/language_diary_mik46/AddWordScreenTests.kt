package uk.ac.aber.dcs.cs31620.language_diary_mik46

import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.lifecycle.viewmodel.compose.viewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.theme.LanguageDiaryTheme

class AddWordScreenTests {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    private lateinit var database: UserWordsViewModel
    private lateinit var testList : List<UserWord>

    @Before
    fun listSetUp(){
        testList = List<UserWord>(6){index ->UserWord("dog${index}", "perro${index}")}
    }

    @Test
    fun addWordNavigateToLibrary() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNode(hasText("dog1")).assertIsDisplayed()


    }

    @Test
    fun addWordButtonDisabledUntilInput() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onAllNodesWithText("Add Word").onFirst().assertIsNotEnabled()
        composeTestRule.onNodeWithText("English").performTextInput("100")
        composeTestRule.onNodeWithText("Welsh").performTextInput("200")
        composeTestRule.onAllNodesWithText("Add Word").onFirst().assertIsEnabled()
    }

    @Test
    fun addWordButtonDisabledWithInvalidInput() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onAllNodesWithText("Add Word").onFirst().assertIsNotEnabled()
        composeTestRule.onNodeWithText("English").performTextInput("     ")
        composeTestRule.onNodeWithText("Welsh").performTextInput("     ")
        composeTestRule.onAllNodesWithText("Add Word").onFirst().assertIsNotEnabled()
    }
    @Test
    fun addWordOnlyOneOfEachWord() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        //dog1 is already in the database so pressing add word will not put it into library/database
        composeTestRule.onNodeWithText("English").performTextInput("dog1")
        composeTestRule.onNodeWithText("Welsh").performTextInput("dog1")
        composeTestRule.onAllNodesWithText("Add Word").onFirst().performClick()
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onAllNodesWithText("dog1").assertCountEquals(1)
    }

    @Test
    fun addWordOnlyOneOfEachWordWithCapitalization() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        //dog1 is already in the database and caps should not matter
        composeTestRule.onNodeWithText("English").performTextInput("DOg1")
        composeTestRule.onNodeWithText("Welsh").performTextInput("dog1")
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onAllNodesWithText("dog1", ignoreCase = true).assertCountEquals(1)
    }

    @Test
    fun addWordOpenNavBar() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithContentDescription("Navigation menu").performClick()

    }

    @Test
    fun addWordToChangeLanguage() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        //dog1 is already in the database and caps should not matter
        composeTestRule.onNodeWithContentDescription("Navigation menu").performClick()
        composeTestRule.onNodeWithContentDescription("Change Language").performClick()
        composeTestRule.onNodeWithText("Confirm").performClick()
        composeTestRule.onNode(hasText("Welcome!")).assertIsDisplayed()
    }

    @Test
    fun addWordChangeLanguageNewLanguageCheck() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        //dog1 is already in the database and caps should not matter
        composeTestRule.onNodeWithContentDescription("Navigation menu").performClick()
        composeTestRule.onNodeWithContentDescription("Change Language").performClick()
        composeTestRule.onNodeWithText("Confirm").performClick()

        composeTestRule.onNodeWithText("First Language").performTextInput("newFirst")
        composeTestRule.onNodeWithText("Second Language").performTextInput("newSecond")
        composeTestRule.onNodeWithText("Start Learning!").performClick()

        composeTestRule.onNodeWithText("newFirst").assertIsDisplayed()
        composeTestRule.onNodeWithText("newSecond").assertIsDisplayed()
    }

    @Test
    fun addWordChangeLanguageNewLanguageCheckTwice() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        //dog1 is already in the database and caps should not matter
        composeTestRule.onNodeWithContentDescription("Navigation menu").performClick()
        composeTestRule.onNodeWithContentDescription("Change Language").performClick()
        composeTestRule.onNodeWithText("Confirm").performClick()

        composeTestRule.onNodeWithText("First Language").performTextInput("newFirst")
        composeTestRule.onNodeWithText("Second Language").performTextInput("newSecond")
        composeTestRule.onNodeWithText("Start Learning!").performClick()

        composeTestRule.onNodeWithText("newFirst").assertIsDisplayed()
        composeTestRule.onNodeWithText("newSecond").assertIsDisplayed()

        composeTestRule.onNodeWithContentDescription("Navigation menu").performClick()
        composeTestRule.onNodeWithContentDescription("Change Language").performClick()
        composeTestRule.onNodeWithText("Confirm").performClick()

        composeTestRule.onNodeWithText("First Language").performTextInput("newFirst2")
        composeTestRule.onNodeWithText("Second Language").performTextInput("newSecond2")
        composeTestRule.onNodeWithText("Start Learning!").performClick()

        composeTestRule.onNodeWithText("newFirst2").assertIsDisplayed()
        composeTestRule.onNodeWithText("newSecond2").assertIsDisplayed()
    }

    @Test
    fun addWordToHelp() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        //dog1 is already in the database and caps should not matter
        composeTestRule.onNodeWithContentDescription("Navigation menu").performClick()
        composeTestRule.onNodeWithContentDescription("Help").performClick()
        composeTestRule.onNodeWithText("Close").performClick()

    }

    @Test
    fun addWordToFeedBack() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        //dog1 is already in the database and caps should not matter
        composeTestRule.onNodeWithContentDescription("Navigation menu").performClick()
        composeTestRule.onNodeWithContentDescription("Feedback").performClick()
        composeTestRule.onNodeWithText("Close").performClick()

    }

    @Test
    fun addWordToExit() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        //dog1 is already in the database and caps should not matter
        composeTestRule.onNodeWithContentDescription("Navigation menu").performClick()
        composeTestRule.onNodeWithContentDescription("Exit").performClick()
        composeTestRule.onNode(hasText("Confirm")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Close").performClick()

    }

    @Test
    fun addWordNavigateToTests() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        //Espresso.onView(withId(R.id.text_field)).perform(typeText("100"), closeSoftKeyboard())


    }

    @Test
    fun addWordToLibrary() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("English").performTextInput("100")
        composeTestRule.onNodeWithText("Welsh").performTextInput("200")
        composeTestRule.onAllNodesWithText("Add Word").onFirst().performClick()
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNode(hasText("100")).assertIsDisplayed()


    }

    @Test
    fun addWordToFirstLanguageEmpty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("English").performTextInput("")
        composeTestRule.onNodeWithText("Welsh").performTextInput("200")
        composeTestRule.onAllNodesWithText("Add Word").onFirst().assertIsNotEnabled()
    }

    @Test
    fun addWordToSecondLanguageEmpty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("English").performTextInput("100")
        composeTestRule.onNodeWithText("Welsh").performTextInput("")
        composeTestRule.onAllNodesWithText("Add Word").onFirst().assertIsNotEnabled()
    }
}