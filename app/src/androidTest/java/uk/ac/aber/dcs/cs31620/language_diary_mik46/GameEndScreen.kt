package uk.ac.aber.dcs.cs31620.language_diary_mik46

import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.lifecycle.viewmodel.compose.viewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.theme.LanguageDiaryTheme

class GameEndScreen {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    private lateinit var database: UserWordsViewModel
    private lateinit var testList : List<UserWord>

    @Before
    fun listSetUp(){
        testList = List<UserWord>(1){ index -> UserWord("${index}", "${index}") }
    }

    private fun getToHangmanTestOnHard(){
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
    }


    private fun guess(){
        composeTestRule.onNode(hasText("Guess") and hasClickAction()).performClick()
        //sleep due to animations
        Thread.sleep(500)
    }

    @Composable
    fun TestContentSetup(){
        database = viewModel()
        database.deleteAllFromDatabase()
        LanguageDiaryTheme{
            Surface(
                modifier = Modifier.fillMaxSize(),
                color = MaterialTheme.colorScheme.background
            ) {
                CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
            }
        }
    }

    @Test
    fun spellingCorrectElapsedTimeTestWait60Seconds() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToHangmanTestOnHard()
        composeTestRule.onNodeWithText("_").assertIsDisplayed()
        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("0")
        Thread.sleep(60000)


        guess()
        composeTestRule.onNodeWithText("Questions Correct:").assertIsDisplayed()
        composeTestRule.onNodeWithText("01:",substring = true).assertIsDisplayed()
        composeTestRule.onNodeWithText("00:00").assertDoesNotExist()
    }

    @Test
    fun correctElapsedTimeTestWait5Seconds() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }

        getToHangmanTestOnHard()
        composeTestRule.onNodeWithText("_").assertIsDisplayed()

        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("0")
        Thread.sleep(5000)
        guess()

        composeTestRule.onNodeWithText("Questions Correct:").assertIsDisplayed()
        composeTestRule.onNodeWithText("Time Taken:").assertIsDisplayed()
        composeTestRule.onNodeWithText("00:",substring = true).assertIsDisplayed()
        composeTestRule.onNodeWithText("00:00").assertDoesNotExist()
    }

    @Test
    fun getToEndScreenWinFromHangman(){
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }

        getToHangmanTestOnHard()
        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("0")
        guess()
        composeTestRule.onNodeWithText("Test Passed").assertIsDisplayed()

    }

    @Test
    fun getToEndScreenLoseFromHangman(){
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }

        getToHangmanTestOnHard()
        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("z")
        guess()
        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("z")
        guess()
        composeTestRule.onNodeWithText("Test Failed").assertIsDisplayed()

    }

    @Test
    fun getToEndScreenCheckCorrectAnswersWin(){
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }

        getToHangmanTestOnHard()
        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("z")
        guess()
        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("z")
        guess()
        composeTestRule.onNodeWithText("0 / 1").assertIsDisplayed()

    }

    @Test
    fun getToEndScreenCheckCorrectAnswersLose(){
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }

        getToHangmanTestOnHard()
        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("0")
        guess()
        composeTestRule.onNodeWithText("1 / 1").assertIsDisplayed()

    }

    @Test
    fun getToEndScreenAndCheckCorrectDifficultyHard(){
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }

        getToHangmanTestOnHard()
        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("0")
        guess()
        composeTestRule.onNodeWithText("HARD").assertIsDisplayed()


    }

    @Test
    fun getToEndScreenAndCheckCorrectDifficultyMedium(){
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()

        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("0")
        guess()
        composeTestRule.onNodeWithText("MEDIUM").assertIsDisplayed()


    }

    @Test
    fun getToEndScreenAndCheckCorrectDifficultyEasy(){
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }

        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()

        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("0")
        guess()
        composeTestRule.onNodeWithText("EASY").assertIsDisplayed()


    }

}