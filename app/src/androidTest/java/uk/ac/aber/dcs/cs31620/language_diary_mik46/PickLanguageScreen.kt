package uk.ac.aber.dcs.cs31620.language_diary_mik46

import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.lifecycle.viewmodel.compose.viewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.pick_language.PickLanguageScreen
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.theme.LanguageDiaryTheme


class PickLanguageScreen {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Test
    fun pickNewLanguageTest() {
        // Start the app
        composeTestRule.activity.setContent{
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    PickLanguageScreen(activity = composeTestRule.activity)
                }
            }
        }
        composeTestRule.onNodeWithText("First Language").performTextInput("newFirst")
        composeTestRule.onNodeWithText("Second Language").performTextInput("newSecond")
        composeTestRule.onNodeWithText("Start Learning!").performClick()
        composeTestRule.onNode(hasText("newFirst")).assertIsDisplayed()
        composeTestRule.onNode(hasText("newSecond")).assertIsDisplayed()
    }

    @Test
    fun pickNewLanguageBothEmpty() {
        // Start the app
        composeTestRule.activity.setContent{
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    PickLanguageScreen(activity = composeTestRule.activity)
                }
            }
        }
        composeTestRule.onNodeWithText("First Language").performTextInput("")
        composeTestRule.onNodeWithText("Second Language").performTextInput("")
        composeTestRule.onNodeWithText("Start Learning!").performClick()
        composeTestRule.onNode(hasText("Welcome!")).assertIsDisplayed()

    }

    @Test
    fun pickNewLanguageFirstEmpty() {
        // Start the app
        composeTestRule.activity.setContent{
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    PickLanguageScreen(activity = composeTestRule.activity)
                }
            }
        }
        composeTestRule.onNodeWithText("First Language").performTextInput("")
        composeTestRule.onNodeWithText("Second Language").performTextInput("22")
        composeTestRule.onNodeWithText("Start Learning!").performClick()
        composeTestRule.onNode(hasText("Welcome!")).assertIsDisplayed()

    }

    @Test
    fun pickNewLanguageSecondEmpty() {
        // Start the app
        composeTestRule.activity.setContent{
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    PickLanguageScreen(activity = composeTestRule.activity)
                }
            }
        }
        composeTestRule.onNodeWithText("First Language").performTextInput("11")
        composeTestRule.onNodeWithText("Second Language").performTextInput("")
        composeTestRule.onNodeWithText("Start Learning!").performClick()
        composeTestRule.onNode(hasText("Welcome!")).assertIsDisplayed()

    }

    @Test
    fun pickNewLanguageFirstSpace() {
        // Start the app
        composeTestRule.activity.setContent{
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    PickLanguageScreen(activity = composeTestRule.activity)
                }
            }
        }
        composeTestRule.onNodeWithText("First Language").performTextInput(" ")
        composeTestRule.onNodeWithText("Second Language").performTextInput("33")
        composeTestRule.onNodeWithText("Start Learning!").performClick()
        composeTestRule.onNode(hasText("Welcome!")).assertIsDisplayed()

    }

    @Test
    fun pickNewLanguageSecondSpace() {
        // Start the app
        composeTestRule.activity.setContent{
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    PickLanguageScreen(activity = composeTestRule.activity)
                }
            }
        }
        composeTestRule.onNodeWithText("First Language").performTextInput("123")
        composeTestRule.onNodeWithText("Second Language").performTextInput("  ")
        composeTestRule.onNodeWithText("Start Learning!").performClick()
        composeTestRule.onNode(hasText("Welcome!")).assertIsDisplayed()

    }

    @Test
    fun pickNewLanguageBothSpaces() {
        // Start the app
        composeTestRule.activity.setContent{
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    PickLanguageScreen(activity = composeTestRule.activity)
                }
            }
        }
        composeTestRule.onNodeWithText("First Language").performTextInput("    ")
        composeTestRule.onNodeWithText("Second Language").performTextInput("    ")
        composeTestRule.onNodeWithText("Start Learning!").performClick()
        composeTestRule.onNode(hasText("Welcome!")).assertIsDisplayed()

    }

    @Test
    fun pickNewLanguageBothSpacesThenBothFilled() {
        // Start the app
        composeTestRule.activity.setContent{
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    PickLanguageScreen(activity = composeTestRule.activity)
                }
            }
        }
        composeTestRule.onNodeWithText("First Language").performTextInput("    ")
        composeTestRule.onNodeWithText("Second Language").performTextInput("    ")
        composeTestRule.onNodeWithText("Start Learning!").performClick()
        composeTestRule.onNode(hasText("Welcome!")).assertIsDisplayed()

        composeTestRule.onNodeWithText("First Language").performTextReplacement("firstLang")
        composeTestRule.onNodeWithText("Second Language").performTextReplacement("second")
        composeTestRule.onNodeWithText("Start Learning!").performClick()
        composeTestRule.onNode(hasText("Welcome!")).assertDoesNotExist()
    }

    @Test
    fun pickNewLanguageTextInputLimit() {
        // Start the app
        composeTestRule.activity.setContent{
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    PickLanguageScreen(activity = composeTestRule.activity)
                }
            }
        }
        for (i in 0..15){
            composeTestRule.onNodeWithText("First Language").performTextInput("${i}")
        }
        for (i in 16..30){
            composeTestRule.onNodeWithText("Second Language").performTextInput("${i}")
        }
        composeTestRule.onNodeWithText("Start Learning!").performClick()
        composeTestRule.onNode(hasText("Welcome!")).assertDoesNotExist()

        composeTestRule.onNode(hasText("01234567891011")).assertIsDisplayed()
        composeTestRule.onNode(hasText("16171819202122")).assertIsDisplayed()
    }
}