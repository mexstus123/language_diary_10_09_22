package uk.ac.aber.dcs.cs31620.language_diary_mik46

import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.lifecycle.viewmodel.compose.viewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.theme.LanguageDiaryTheme

class TestsScreenTests {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    private lateinit var database: UserWordsViewModel
    private lateinit var testListSize6 : List<UserWord>
    private lateinit var testListSize1 : List<UserWord>
    private lateinit var testListSize10 : List<UserWord>
    private lateinit var testListSize15 : List<UserWord>
    private lateinit var testListSize50 : List<UserWord>
    private lateinit var testListSize0 : List<UserWord>

    @Before
    fun listSetUp(){
        testListSize6 = List<UserWord>(6){ index -> UserWord("dog${index}", "perro${index}") }
        testListSize1 = List<UserWord>(1){ index -> UserWord("dog${index}", "perro${index}") }
        testListSize10 = List<UserWord>(10){ index -> UserWord("dog${index}", "perro${index}") }
        testListSize15 = List<UserWord>(15){ index -> UserWord("dog${index}", "perro${index}") }
        testListSize50 = List<UserWord>(50){ index -> UserWord("dog${index}", "perro${index}") }
        testListSize0 = emptyList<UserWord>()
    }

    @Test
    fun testsOpenNavBar() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithContentDescription("Navigation menu").performClick()
    }

    @Test
    fun testsNavigateToLibrary() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNode(hasText("dog1")).assertIsDisplayed()
    }
    @Test
    fun testsNavigateToAddWord() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Add Word").performClick()
        composeTestRule.onNode(hasText("English")).assertIsDisplayed()
    }

    @Test
    fun tests6WordsMultiEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertDoesNotExist()
    }

    @Test
    fun tests6WordsMultiMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertDoesNotExist()
    }

    @Test
    fun tests6WordsMultiHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertDoesNotExist()
    }

    @Test
    fun tests6WordsSpellingEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertDoesNotExist()
    }

    @Test
    fun tests6WordsSpellingMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
    }

    @Test
    fun tests6WordsSpellingHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
    }

    @Test
    fun tests6WordsHangmanEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests6WordsHangmanMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests6WordsHangmanHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests6WordsPairingEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertDoesNotExist()
    }

    @Test
    fun tests6WordsPairingMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertDoesNotExist()
    }

    @Test
    fun tests6WordsPairingHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize6)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertDoesNotExist()
    }


//######################################################################################################################################################################


    //1 word tests


//######################################################################################################################################################################


    @Test
    fun tests1WordsPairingEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize1)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
    }

    @Test
    fun tests1WordsPairingMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize1)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
    }

    @Test
    fun tests1WordsPairingHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize1)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
    }




    @Test
    fun tests1WordsHangmanEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize1)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests1WordsHangmanMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize1)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests1WordsHangmanHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize1)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests1WordsMultiEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize1)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
    }

    @Test
    fun tests1WordsMultiMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize1)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
    }

    @Test
    fun tests1WordsMultiHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize1)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
    }

    @Test
    fun tests1WordsSpellingEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize1)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
    }

    @Test
    fun tests1WordsSpellingMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize1)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
    }

    @Test
    fun tests1WordsSpellingHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize1)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
    }

//######################################################################################################################################################################


    //10 words


//######################################################################################################################################################################



    @Test
    fun tests10WordsPairingEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize10)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertDoesNotExist()
    }

    @Test
    fun tests10WordsPairingMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize10)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertDoesNotExist()
    }

    @Test
    fun tests10WordsPairingHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize10)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertDoesNotExist()
    }


    @Test
    fun tests10WordsHangmanEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize10)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests10WordsHangmanMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize10)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests10WordsHangmanHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize10)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests10WordsMultiEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize10)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertDoesNotExist()
    }

    @Test
    fun tests10WordsMultiMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize10)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertDoesNotExist()
    }

    @Test
    fun tests10WordsMultiHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize10)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertDoesNotExist()
    }

    @Test
    fun tests10WordsSpellingEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize10)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertDoesNotExist()
    }

    @Test
    fun tests10WordsSpellingMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize10)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertDoesNotExist()
    }

    @Test
    fun tests10WordsSpellingHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize10)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
    }



//######################################################################################################################################################################


    //15 words tests


//######################################################################################################################################################################



    @Test
    fun tests15WordsPairingEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize15)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertDoesNotExist()
    }

    @Test
    fun tests15WordsPairingMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize15)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertDoesNotExist()
    }

    @Test
    fun tests15WordsPairingHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize15)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertDoesNotExist()
    }


    @Test
    fun tests15WordsHangmanEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize15)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests15WordsHangmanMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize15)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests15WordsHangmanHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize15)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests15WordsMultiEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize15)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertDoesNotExist()
    }

    @Test
    fun tests15WordsMultiMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize15)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertDoesNotExist()
    }

    @Test
    fun tests15WordsMultiHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize15)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertDoesNotExist()
    }

    @Test
    fun tests15WordsSpellingEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize15)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertDoesNotExist()
    }

    @Test
    fun tests15WordsSpellingMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize15)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertDoesNotExist()
    }

    @Test
    fun tests15WordsSpellingHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize15)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertDoesNotExist()
    }

//######################################################################################################################################################################


    //50 words


//######################################################################################################################################################################


    @Test
    fun tests50WordsPairingEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize50)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertDoesNotExist()
    }

    @Test
    fun tests50WordsPairingMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize50)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertDoesNotExist()
    }

    @Test
    fun tests50WordsPairingHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize50)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertDoesNotExist()
    }


    @Test
    fun tests50WordsHangmanEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize50)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests50WordsHangmanMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize50)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests50WordsHangmanHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize50)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertDoesNotExist()
    }

    @Test
    fun tests50WordsMultiEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize50)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertDoesNotExist()
    }

    @Test
    fun tests50WordsMultiMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize50)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertDoesNotExist()
    }

    @Test
    fun tests50WordsMultiHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize50)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertDoesNotExist()
    }

    @Test
    fun tests50WordsSpellingEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize50)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertDoesNotExist()
    }

    @Test
    fun tests50WordsSpellingMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize50)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertDoesNotExist()
    }

    @Test
    fun tests50WordsSpellingHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize50)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertDoesNotExist()
    }


//######################################################################################################################################################################


    //0 words


//######################################################################################################################################################################


    @Test
    fun tests0WordsPairingEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize0)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
    }

    @Test
    fun tests0WordsPairingMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize0)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
    }

    @Test
    fun tests0WordsPairingHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize0)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Pairing").performClick()
        composeTestRule.onNode(hasText("Pairing")).assertIsDisplayed()
    }


    @Test
    fun tests0WordsHangmanEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize0)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
    }

    @Test
    fun tests0WordsHangmanMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize0)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
    }

    @Test
    fun tests0WordsHangmanHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize0)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNode(hasText("Hangman")).assertIsDisplayed()
    }

    @Test
    fun tests0WordsMultiEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize0)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
    }

    @Test
    fun tests0WordsMultiMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize0)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
    }

    @Test
    fun tests0WordsMultiHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize0)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
        composeTestRule.onNode(hasText("Multiple Choice")).assertIsDisplayed()
    }

    @Test
    fun tests0WordsSpellingEasyDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize0)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
    }

    @Test
    fun tests0WordsSpellingMediumDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize0)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
    }

    @Test
    fun tests0WordsSpellingHardDifficulty() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testListSize0)
                }
            }
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Spelling").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
    }
}
