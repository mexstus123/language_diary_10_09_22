package uk.ac.aber.dcs.cs31620.language_diary_mik46

import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.lifecycle.viewmodel.compose.viewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.theme.LanguageDiaryTheme

class LibraryScreenTests {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    private lateinit var database: UserWordsViewModel
    private lateinit var testList : List<UserWord>

    @Before
    fun listSetUp(){
        testList = listOf(
            UserWord("dog", "ci",true),
            UserWord("cat", "gath",true),
            UserWord("mouse", "llygoden"),
            UserWord("bird", "aderyn"),
            UserWord("fish", "pysgodyn",true),
            UserWord("snake", "nadroedd")
        )
    }
    @Test
    fun libraryDBSetupTest() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("dog").assertIsDisplayed()
        composeTestRule.onNodeWithText("cat").assertIsDisplayed()
        composeTestRule.onNodeWithText("mouse").assertIsDisplayed()
        composeTestRule.onNodeWithText("aderyn").assertIsDisplayed()
        composeTestRule.onNodeWithText("pysgodyn").assertIsDisplayed()
        composeTestRule.onNodeWithText("snake").assertIsDisplayed()
    }

    @Test
    fun libraryNavigateToAddWord() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("Add Word").performClick()
        composeTestRule.onNode(hasText("English")).assertIsDisplayed()
    }

    @Test
    fun libraryNavigateToTests() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNode(hasText("Spelling")).assertIsDisplayed()
    }
    @Test
    fun libraryOpenNavBar() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithContentDescription("Navigation menu").performClick()
    }

    @Test
    fun librarySearchWithNoFilters() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("Search").performTextInput("do")
        composeTestRule.onNode(hasText("dog")).assertIsDisplayed()
        composeTestRule.onNode(hasText("ci")).assertIsDisplayed()
        composeTestRule.onNodeWithText("Search").performTextReplacement("c")
        composeTestRule.onNode(hasText("cat")).assertIsDisplayed()
        composeTestRule.onNode(hasText("gath")).assertIsDisplayed()
        composeTestRule.onNode(hasText("ci")).assertIsDisplayed()

    }

    @Test
    fun librarySearchWithLanguageEnglishFilters() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("All Languages").performClick()
        composeTestRule.onNodeWithText("English").performClick()
        composeTestRule.onNodeWithText("Search").performTextInput("do")
        composeTestRule.onNode(hasText("dog")).assertIsDisplayed()
        composeTestRule.onNode(hasText("ci")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNodeWithText("Search").performTextReplacement("c")
        composeTestRule.onNode(hasText("cat")).assertIsDisplayed()
        composeTestRule.onNode(hasText("gath")and hasNoClickAction()).assertDoesNotExist()

    }

    @Test
    fun librarySearchWithLanguageWelshFilters() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("All Languages").performClick()
        composeTestRule.onNodeWithText("Welsh").performClick()
        composeTestRule.onNodeWithText("Search").performTextInput("c")
        composeTestRule.onNode(hasText("ci")).assertIsDisplayed()
        composeTestRule.onNode(hasText("dog")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNodeWithText("Search").performTextReplacement("g")
        composeTestRule.onNode(hasText("gath")).assertIsDisplayed()
        composeTestRule.onNode(hasText("cat")and hasNoClickAction()).assertDoesNotExist()

    }

    @Test
    fun librarySearchWithFavouritesFilters() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("All Words").performClick()
        composeTestRule.onNodeWithText("Favourites").performClick()
        composeTestRule.onNode(hasText("bird")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNodeWithText("Search").performTextInput("c")
        composeTestRule.onNode(hasText("ci")).assertIsDisplayed()
        composeTestRule.onNode(hasText("cat")).assertIsDisplayed()

    }

    @Test
    fun librarySearchWithNoFavouritesFilters() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("All Words").performClick()
        composeTestRule.onNodeWithText("No Favourites").performClick()
        composeTestRule.onNode(hasText("dog")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNodeWithText("Search").performTextInput("r")
        composeTestRule.onNode(hasText("bird")).assertIsDisplayed()
        composeTestRule.onNode(hasText("nadroedd")).assertIsDisplayed()
        composeTestRule.onNode(hasText("snake")).assertIsDisplayed()

    }

    @Test
    fun librarySearchWithFavouritesAndWelshFilters() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("All Words").performClick()
        composeTestRule.onNodeWithText("Favourites").performClick()
        composeTestRule.onNodeWithText("All Languages").performClick()
        composeTestRule.onNodeWithText("Welsh").performClick()
        composeTestRule.onNode(hasText("dog")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNode(hasText("nadroedd")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNodeWithText("Search").performTextInput("o")
        composeTestRule.onNode(hasText("pysgodyn")).assertIsDisplayed()


    }

    @Test
    fun librarySearchWithNoFavouritesAndWelshFilters() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("All Words").performClick()
        composeTestRule.onNodeWithText("No Favourites").performClick()
        composeTestRule.onNodeWithText("All Languages").performClick()
        composeTestRule.onNodeWithText("Welsh").performClick()
        composeTestRule.onNode(hasText("dog")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNode(hasText("pysgodyn")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNodeWithText("Search").performTextInput("o")
        composeTestRule.onNode(hasText("nadroedd")).assertIsDisplayed()


    }

    @Test
    fun librarySearchWithNoFavouritesAndEnglishFilters() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("All Words").performClick()
        composeTestRule.onNodeWithText("No Favourites").performClick()
        composeTestRule.onNodeWithText("All Languages").performClick()
        composeTestRule.onNodeWithText("English").performClick()
        composeTestRule.onNode(hasText("dog")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNode(hasText("nadroedd")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNodeWithText("Search").performTextInput("o")
        composeTestRule.onNode(hasText("mouse")).assertIsDisplayed()


    }

    @Test
    fun librarySearchWithFavouritesAndEnglishFilters() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("All Words").performClick()
        composeTestRule.onNodeWithText("Favourites").performClick()
        composeTestRule.onNodeWithText("All Languages").performClick()
        composeTestRule.onNodeWithText("English").performClick()
        composeTestRule.onNode(hasText("snake")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNode(hasText("ci")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNodeWithText("Search").performTextInput("o")
        composeTestRule.onNode(hasText("dog")).assertIsDisplayed()
        composeTestRule.onNode(hasText("ci")and hasNoClickAction()).assertDoesNotExist()


    }

    @Test
    fun libraryFavouriteIconButtonFavouriteToUnFavouriteMenu() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("All Words").performClick()
        composeTestRule.onNodeWithText("Favourites").performClick()
        composeTestRule.onNode(hasText("dog")).assertIsDisplayed()
        composeTestRule.onNodeWithContentDescription("dog menu").performClick()
        composeTestRule.onNodeWithContentDescription("dog heart").performClick()
        composeTestRule.onNode(hasText("dog")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNode(hasText("cat")).assertIsDisplayed()
        composeTestRule.onNodeWithContentDescription("cat menu").performClick()
        composeTestRule.onNodeWithContentDescription("cat heart").performClick()
        composeTestRule.onNode(hasText("cat")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNodeWithText("Favourites").performClick()
        composeTestRule.onNodeWithText("No Favourites").performClick()
        composeTestRule.onNode(hasText("dog")).assertIsDisplayed()
        composeTestRule.onNode(hasText("cat")).assertIsDisplayed()



    }
    @Test
    fun libraryFavouriteIconButtonUnFavouriteToFavouriteMenu() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onNodeWithText("All Words").performClick()
        composeTestRule.onNodeWithText("No Favourites").performClick()

        composeTestRule.onNode(hasText("mouse")).assertIsDisplayed()
        composeTestRule.onNodeWithContentDescription("mouse menu").performClick()
        composeTestRule.onNodeWithContentDescription("mouse heart").performClick()
        composeTestRule.onNode(hasText("mouse")and hasNoClickAction()).assertDoesNotExist()

        composeTestRule.onNode(hasText("bird")).assertIsDisplayed()
        composeTestRule.onNodeWithContentDescription("bird menu").performClick()
        composeTestRule.onNodeWithContentDescription("bird heart").performClick()
        composeTestRule.onNode(hasText("bird")and hasNoClickAction()).assertDoesNotExist()

        composeTestRule.onNodeWithText("No Favourites").performClick()
        composeTestRule.onNodeWithText("Favourites").performClick()
        composeTestRule.onNode(hasText("mouse")).assertIsDisplayed()
        composeTestRule.onNode(hasText("bird")).assertIsDisplayed()



    }

    @Test
    fun libraryAddNewWordAndFavourite() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("English").performTextInput("newAnimal")
        composeTestRule.onNodeWithText("Welsh").performTextInput("WelshAnimal")
        composeTestRule.onAllNodesWithText("Add Word").onFirst().performClick()
        composeTestRule.onNodeWithText("Library").performClick()

        composeTestRule.onNode(hasText("newAnimal")).performScrollTo()

        composeTestRule.onNodeWithContentDescription("newAnimal menu").performClick()
        composeTestRule.onNodeWithContentDescription("newAnimal heart").performClick()

        composeTestRule.onNodeWithText("All Words").performClick()
        composeTestRule.onNodeWithText("Favourites").performClick()

        composeTestRule.onNode(hasText("newAnimal")).assertIsDisplayed()

        composeTestRule.onNodeWithText("Favourites").performClick()
        composeTestRule.onNodeWithText("No Favourites").performClick()

        composeTestRule.onNode(hasText("newAnimal")and hasNoClickAction()).assertDoesNotExist()



    }

    @Test
    fun libraryAddNewWordAndDelete() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("English").performTextInput("newAnimal")
        composeTestRule.onNodeWithText("Welsh").performTextInput("WelshAnimal")
        composeTestRule.onAllNodesWithText("Add Word").onFirst().performClick()
        composeTestRule.onNodeWithText("Library").performClick()

        composeTestRule.onNode(hasText("newAnimal")).performScrollTo()

        composeTestRule.onNodeWithContentDescription("newAnimal menu").performClick()
        composeTestRule.onNodeWithContentDescription("newAnimal delete").performClick()
        composeTestRule.onNode(hasText("Delete") and hasClickAction() ).performClick()

        composeTestRule.onNode(hasText("newAnimal")and hasNoClickAction()).assertDoesNotExist()

    }

    @Test
    fun libraryDeleteMultiple() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onAllNodes(hasNoClickAction()).assertCountEquals(26)

        composeTestRule.onNodeWithContentDescription("dog menu").performClick()
        composeTestRule.onNodeWithContentDescription("dog delete").performClick()
        composeTestRule.onNode(hasText("Delete") and hasClickAction() ).performClick()

        composeTestRule.onAllNodes(hasNoClickAction()).assertCountEquals(24)
        composeTestRule.onNode(hasText("dog")and hasNoClickAction()).assertDoesNotExist()

        composeTestRule.onNodeWithContentDescription("cat menu").performClick()
        composeTestRule.onNodeWithContentDescription("cat delete").performClick()
        composeTestRule.onNode(hasText("Delete") and hasClickAction() ).performClick()

        composeTestRule.onAllNodes(hasNoClickAction()).assertCountEquals(22)
        composeTestRule.onNode(hasText("cat")and hasNoClickAction()).assertDoesNotExist()

        composeTestRule.onNodeWithContentDescription("mouse menu").performClick()
        composeTestRule.onNodeWithContentDescription("mouse delete").performClick()
        composeTestRule.onNode(hasText("Delete") and hasClickAction() ).performClick()

        composeTestRule.onAllNodes(hasNoClickAction()).assertCountEquals(20)
        composeTestRule.onNode(hasText("mouse")and hasNoClickAction()).assertDoesNotExist()
    }

    @Test
    fun libraryDeleteAllAddWord() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()

        composeTestRule.onNodeWithContentDescription("dog menu").performClick()
        composeTestRule.onNodeWithContentDescription("dog delete").performClick()
        composeTestRule.onNode(hasText("Delete") and hasClickAction() ).performClick()

        composeTestRule.onAllNodes(hasNoClickAction()).assertCountEquals(24)
        composeTestRule.onNode(hasText("dog")and hasNoClickAction()).assertDoesNotExist()

        composeTestRule.onNodeWithContentDescription("cat menu").performClick()
        composeTestRule.onNodeWithContentDescription("cat delete").performClick()
        composeTestRule.onNode(hasText("Delete") and hasClickAction() ).performClick()


        composeTestRule.onNodeWithContentDescription("mouse menu").performClick()
        composeTestRule.onNodeWithContentDescription("mouse delete").performClick()
        composeTestRule.onNode(hasText("Delete") and hasClickAction() ).performClick()

        composeTestRule.onNodeWithContentDescription("snake menu").performClick()
        composeTestRule.onNodeWithContentDescription("snake delete").performClick()
        composeTestRule.onNode(hasText("Delete") and hasClickAction() ).performClick()

        composeTestRule.onNodeWithContentDescription("bird menu").performClick()
        composeTestRule.onNodeWithContentDescription("bird delete").performClick()
        composeTestRule.onNode(hasText("Delete") and hasClickAction() ).performClick()

        composeTestRule.onNodeWithContentDescription("fish menu").performClick()
        composeTestRule.onNodeWithContentDescription("fish delete").performClick()
        composeTestRule.onNode(hasText("Delete") and hasClickAction() ).performClick()

        composeTestRule.onAllNodes(hasNoClickAction()).assertCountEquals(14)

        composeTestRule.onNode(hasText("cat")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNode(hasText("mouse")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNode(hasText("snake")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNode(hasText("bird")and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNode(hasText("fish")and hasNoClickAction()).assertDoesNotExist()


        composeTestRule.onNodeWithText("Add Word").performClick()
        composeTestRule.onNodeWithText("English").performTextInput("newAnimal")
        composeTestRule.onNodeWithText("Welsh").performTextInput("WelshAnimal")
        composeTestRule.onAllNodesWithText("Add Word").onFirst().performClick()
        composeTestRule.onNodeWithText("Library").performClick()

        composeTestRule.onNode(hasText("newAnimal")).assertIsDisplayed()
        composeTestRule.onAllNodes(hasNoClickAction()).assertCountEquals(16)

        composeTestRule.onNodeWithContentDescription("newAnimal menu").performClick()
        composeTestRule.onNodeWithContentDescription("newAnimal delete").performClick()
        composeTestRule.onNode(hasText("Delete") and hasClickAction() ).performClick()

        composeTestRule.onAllNodes(hasNoClickAction()).assertCountEquals(14)
    }

    @Test
    fun libraryChangeLanguageRemoveAllCheck() {
        // Start the app
        composeTestRule.activity.setContent{
            database = viewModel()
            database.deleteAllFromDatabase()
            LanguageDiaryTheme{
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
                }
            }
        }
        composeTestRule.onNodeWithText("Library").performClick()
        composeTestRule.onAllNodes(hasNoClickAction()).assertCountEquals(26)

        composeTestRule.onNodeWithContentDescription("Navigation menu").performClick()
        composeTestRule.onNodeWithContentDescription("Change Language").performClick()
        composeTestRule.onNodeWithText("Confirm").performClick()

        composeTestRule.onNode(hasText("Welcome!")).assertIsDisplayed()

        composeTestRule.onNodeWithText("First Language").performTextInput("newFirst")
        composeTestRule.onNodeWithText("Second Language").performTextInput("newSecond")
        composeTestRule.onNodeWithText("Start Learning!").performClick()
        composeTestRule.onNodeWithText("Library").performClick()

        composeTestRule.onAllNodes(hasNoClickAction()).assertCountEquals(14)


    }



}

