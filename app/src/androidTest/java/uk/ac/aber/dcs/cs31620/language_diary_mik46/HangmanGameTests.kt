package uk.ac.aber.dcs.cs31620.language_diary_mik46

import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.lifecycle.viewmodel.compose.viewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.theme.LanguageDiaryTheme

class HangmanGameTests {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    private lateinit var database: UserWordsViewModel
    private lateinit var testList : List<UserWord>

    @Before
    fun listSetUp(){
        testList = List<UserWord>(1){ index -> UserWord("dog${index}", "dog${index}") }
    }

    private fun getToHangmanTestOnEasy(){
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
    }

    private fun goBackToTestsScreenAndConfirm(){
        composeTestRule.onNodeWithContentDescription("Go Back Arrow").performClick()
        composeTestRule.onNodeWithText("Spelling").assertIsDisplayed()
        composeTestRule.onNodeWithText("Pairing").assertIsDisplayed()
    }


    private fun guess(){
        composeTestRule.onNode(hasText("Guess") and hasClickAction()).performClick()
        //sleep due to animations
        Thread.sleep(500)
    }

    @Composable
    fun TestContentSetup(){
        database = viewModel()
        database.deleteAllFromDatabase()
        LanguageDiaryTheme{
            Surface(
                modifier = Modifier.fillMaxSize(),
                color = MaterialTheme.colorScheme.background
            ) {
                CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
            }
        }
    }

    @Test
    fun hangmanBackNavigation() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToHangmanTestOnEasy()
        goBackToTestsScreenAndConfirm()
    }

    @Test
    fun hangmanConfirmLivesEasy() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToHangmanTestOnEasy()
        composeTestRule.onNodeWithText("7").assertIsDisplayed()
    }

    @Test
    fun hangmanConfirmLivesMedium() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNodeWithText("5").assertIsDisplayed()
        goBackToTestsScreenAndConfirm()
    }

    @Test
    fun hangmanConfirmLivesHard() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Hangman").performClick()
        composeTestRule.onNodeWithText("2").assertIsDisplayed()
        goBackToTestsScreenAndConfirm()
    }

    @Test
    fun hangmanGuessIncorrectlyLifeCounter() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToHangmanTestOnEasy()
        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("z")
        guess()
        composeTestRule.onNodeWithText("6").assertIsDisplayed()
        goBackToTestsScreenAndConfirm()
    }

    @Test
    fun hangmanGuessIncorrectSameLetterUntilGameEnd() {
        // Start the app
        composeTestRule.activity.setContent {
            TestContentSetup()
        }
        getToHangmanTestOnEasy()
        for (i in 0..6){
            composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("z")
            guess()
        }
        composeTestRule.onNodeWithText("Test Failed").assertIsDisplayed()
        goBackToTestsScreenAndConfirm()
    }



    @Test
    fun hangmanTextFieldMaxInputText() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToHangmanTestOnEasy()

        for (i in 0..10){
            composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("${i}")
        }
        composeTestRule.onNode(hasText("0") and hasClickAction()).assertIsDisplayed()
    }

    @Test
    fun hangmanTextFieldSpaceInputText() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToHangmanTestOnEasy()

        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput(" ")
        composeTestRule.onNode(hasText("") and hasClickAction()).assertIsDisplayed()
    }

    @Test
    fun hangmanGuessWinGameConfirmBlankedWord() {
        // Start the app
        composeTestRule.activity.setContent {
            TestContentSetup()
        }
        getToHangmanTestOnEasy()

        composeTestRule.onNode(hasText("____")).assertIsDisplayed()

        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("d")
        guess()
        composeTestRule.onNode(hasText("d___")).assertIsDisplayed()

        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("o")
        guess()
        composeTestRule.onNode(hasText("do__")).assertIsDisplayed()

        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("g")
        guess()
        composeTestRule.onNode(hasText("dog_")).assertIsDisplayed()

        composeTestRule.onNode(hasText("") and hasClickAction()).performTextInput("0")
        guess()

        composeTestRule.onNodeWithText("Test Passed").assertIsDisplayed()
        goBackToTestsScreenAndConfirm()
    }

}