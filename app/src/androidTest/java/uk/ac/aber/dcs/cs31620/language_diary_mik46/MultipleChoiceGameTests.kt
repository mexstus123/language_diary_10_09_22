package uk.ac.aber.dcs.cs31620.language_diary_mik46

import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.lifecycle.viewmodel.compose.viewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.theme.LanguageDiaryTheme

class MultipleChoiceGameTests {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    private lateinit var database: UserWordsViewModel
    private lateinit var testList : List<UserWord>

    @Before
    fun listSetUp(){
        testList = List<UserWord>(5){ index -> UserWord("dog${index}", "ci${index}") }
    }

    private fun getToMultipleChoiceTestOnEasy(){
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Easy").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
    }

    private fun getToMultipleChoiceTestOnMedium(){
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Medium").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
    }

    private fun getToMultipleChoiceTestOnHard(){
        composeTestRule.onNodeWithText("Tests").performClick()
        composeTestRule.onNodeWithText("Hard").performClick()
        composeTestRule.onNodeWithText("Multiple Choice").performClick()
    }

    private fun goBackToTestsScreenAndConfirm(){
        composeTestRule.onNodeWithContentDescription("Go Back Arrow").performClick()
        composeTestRule.onNodeWithText("Spelling").assertIsDisplayed()
        composeTestRule.onNodeWithText("Pairing").assertIsDisplayed()
    }


    private fun goToNextQuestion(){
        composeTestRule.onNode(hasText("Next Question") and hasClickAction()).performClick()
        //sleep due to animations
        Thread.sleep(500)
    }

    @Composable
    fun TestContentSetup(){
        database = viewModel()
        database.deleteAllFromDatabase()
        LanguageDiaryTheme{
            Surface(
                modifier = Modifier.fillMaxSize(),
                color = MaterialTheme.colorScheme.background
            ) {
                CreateUserDataBaseTestSetup("English", "Welsh", activity = composeTestRule.activity, wordList = testList)
            }
        }
    }

    @Test
    fun multipleChoiceBackNavigation() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToMultipleChoiceTestOnEasy()
        composeTestRule.onNodeWithContentDescription("Go Back Arrow").assertIsDisplayed()
        goBackToTestsScreenAndConfirm()
    }

    @Test
    fun multipleChoiceTestBackNavigationForHard() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToMultipleChoiceTestOnHard()

        for (test in 1..12) {
            for (question in 1..test) {
                composeTestRule.onAllNodes(hasClickAction()).onFirst().performClick()
                goToNextQuestion()
            }
            goBackToTestsScreenAndConfirm()
            composeTestRule.onNodeWithText("Multiple Choice").performClick()
        }
    }

    @Test
    fun multipleChoiceTestAllNavigationForMedium() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToMultipleChoiceTestOnMedium()

        for (test in 1..8) {
            for (question in 1..test) {
                composeTestRule.onAllNodes(hasClickAction()).onFirst().performClick()
                goToNextQuestion()
            }
            goBackToTestsScreenAndConfirm()
            composeTestRule.onNodeWithText("Multiple Choice").performClick()
        }
    }

    @Test
    fun multipleChoiceTestAllNavigationForEasy() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToMultipleChoiceTestOnEasy()

        for (test in 1..4) {
            for (question in 1..test) {
                composeTestRule.onAllNodes(hasClickAction()).onFirst().performClick()
                goToNextQuestion()
            }
            goBackToTestsScreenAndConfirm()
            composeTestRule.onNodeWithText("Multiple Choice").performClick()
        }
    }

    @Test
    fun multipleChoiceTestAllButtons() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToMultipleChoiceTestOnEasy()

        for (question in 0..3) {
            composeTestRule.onAllNodes(hasClickAction())[question].performClick()
                goToNextQuestion()
            }

        composeTestRule.onNodeWithText("Time Taken:").assertIsDisplayed()
        }

    @Test
    fun multipleChoiceNoSelectionGuess() {
        // Start the app
        composeTestRule.activity.setContent{
            TestContentSetup()
        }
        getToMultipleChoiceTestOnEasy()

        for (question in 0..8) {
            goToNextQuestion()
        }
        composeTestRule.onNodeWithText("Next Question").assertIsDisplayed()
    }

}