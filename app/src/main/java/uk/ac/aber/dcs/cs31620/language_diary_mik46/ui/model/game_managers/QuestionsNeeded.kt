package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers

/**
 * enum class for constants for the amount of
 * questions needed to play each game type
 * @param standard The standard number of questions needed for the type of game.
 */
enum class QuestionsNeeded(val standard: Int) {
    SPELLING(8),
    MULTIPLE(5),
    HANGMAN(1),
    PAIRING(5)
}