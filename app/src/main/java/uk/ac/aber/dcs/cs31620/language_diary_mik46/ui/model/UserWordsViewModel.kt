package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uk.ac.aber.dcs.cs31620.language_diary_mik46.dataSource.LanguageDiaryRepository

/**
 * A ViewModel class for the UserWords database.
 * Provides methods for adding, deleting, and updating UserWord objects in the database.
 * Also provides methods for retrieving LiveData lists of all words
 * in the database, and of only favourite words in the database.
 */
class UserWordsViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: LanguageDiaryRepository = LanguageDiaryRepository(application)
    private val coroutineScope = CoroutineScope(Dispatchers.IO)

    /**
     * A LiveData list of all UserWord objects in the database.
     */
    var wordsList: LiveData<List<UserWord>> = repository.getAllWords()
        private set

    /**
     * Adds a new UserWord object to the database.
     *
     * @param userWord the UserWord object to be added
     */
    fun addWord(userWord: UserWord){
            coroutineScope.launch {
                repository.insert(userWord)
        }

    }
    /**
     * Updates an existing UserWord object in the database.
     *
     * @param userWord the UserWord object to be updated
     */
    fun updateWord(userWord: UserWord){
        coroutineScope.launch {
            repository.updateWord(userWord)
        }
    }
    /**
     * Get UserWords which are favourites from the database.
     */
    fun getFavourites(): LiveData<List<UserWord>>{
        return repository.getFavouriteWords()
    }

    /**
     * Get UserWords which are not favourites from the database.
     */
    fun getNoFavourites(): LiveData<List<UserWord>> {
        return repository.getNoFavouriteWords()
    }

    /**
     * deletes all from the database.
     */
    fun deleteAllFromDatabase() {
        coroutineScope.launch {
            repository.deleteAll()
        }
    }

    /**
     * Delete a single UserWord which from the database.
     *
     * @param userWord the UserWord object to be deleted
     */
    fun deleteWordFromDatabase(userWord: UserWord) {
        coroutineScope.launch {
            repository.deleteWord(userWord)
        }
    }
}