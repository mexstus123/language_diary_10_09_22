package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.pick_language

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import uk.ac.aber.dcs.cs31620.language_diary_mik46.MainActivity
import uk.ac.aber.dcs.cs31620.language_diary_mik46.R
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.User
import uk.ac.aber.dcs.cs31620.language_diary_mik46.file_manager.createLanguageFile
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.OutlinedTextFieldParams
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.textFieldParameters

/**
 * This composable function creates the UI for the Pick Language screen, which allows the user to choose their primary
 * and secondary languages.
 *
 * @param activity the MainActivity instance that this composable function is being called from
 */
@Composable
fun PickLanguageScreen( activity: MainActivity){
    // A surface container using the 'background' color from the theme
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background
    ) {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
                Image(
        modifier = Modifier
            .fillMaxWidth()
            .height(250.dp),
        painter = painterResource(id = R.drawable.wave_start),
        contentDescription = stringResource(R.string.wave_start),
        contentScale = ContentScale.FillBounds
    )

            Column(modifier = Modifier
                .height(500.dp)
                .fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally) {

                Text(

                    text = stringResource(R.string.welcome),
                    textAlign = TextAlign.Center,
                    fontSize = 35.sp,
                    modifier = Modifier.weight(0.06F),

                )

                Text(
                    text = stringResource(R.string.pick_your_language),
                    textAlign = TextAlign.Center,
                    fontSize = 25.sp,
                    modifier = Modifier.weight(0.06F),
                )

                val firstLanguageChoice = rememberSaveable { mutableStateOf("") }
                OutlinedTextFieldParams(modifier = Modifier
                    .padding(30.dp, 10.dp, 30.dp, 0.dp)
                    .height(65.dp)
                    .fillMaxWidth(),
                    textValue = firstLanguageChoice.value,
                    onValueChange = {
                        textFieldParameters(it, firstLanguageChoice, activity)
                    },
                    stringResource(R.string.first_language)
                )

                val secondLanguageChoice = rememberSaveable { mutableStateOf("") }
                OutlinedTextFieldParams(modifier = Modifier
                    .padding(30.dp, 10.dp, 30.dp, 0.dp)
                    .height(65.dp)
                    .fillMaxWidth(),
                    textValue = secondLanguageChoice.value,
                    onValueChange = {
                        textFieldParameters(it, secondLanguageChoice, activity)
                    },
                    stringResource(R.string.second_language)
                )
                Spacer(modifier = Modifier.height(70.dp))
                Button(
                    elevation = ButtonDefaults.buttonElevation(defaultElevation = 6.dp),
                    onClick = {
                        if (firstLanguageChoice.value != "" && secondLanguageChoice.value != ""){
                            //creating a language file
                            createLanguageFile(activity,
                                User(firstLanguageChoice.value,secondLanguageChoice.value, emptyList())
                            )
                            firstLanguageChoice.value = ""
                            secondLanguageChoice.value = ""
                            //using recreate to send it back to main but as a language file has been created it will go to Add Word screen instead
                            activity.recreate()

                        }},
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(30.dp)
                        .weight(0.1F)
                ) {
                    Text(text = stringResource(R.string.start_learning),
                        fontWeight = FontWeight.Bold,
                        fontSize = 20.sp
                    )
                }
            }
        }

    }
}

