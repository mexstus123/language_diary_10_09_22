package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.test_screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.*
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.User
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.PairingGameManager
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.navigation.Screen
import uk.ac.aber.dcs.cs31620.language_diary_mik46.R
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.MultipleChoiceGameManager

/**
 * This function creates the UI for the Pairing Test screen, which allows the user to test their knowledge
 * of paired primary and secondary language words by selecting pairs of buttons that contain the correct words.
 *
 * @param navController the NavHostController that is used to navigate between screens
 * @param user the User object that contains information about the user's language choices and progress
 */
@Composable
fun PairingTestScreen(navController: NavHostController,  user: User) {

    // Initializes the game manager with the user's randomized questions
    val gameManager = remember { PairingGameManager(user.randomizedQuestions) }
    // Saves the state of the shuffled word list
    val shuffledWords = remember { mutableStateOf(gameManager.getShuffledWordList()) }

    // Saves the state of the list of clickable buttons
    val buttonClickableList = remember { MutableList(11) { 0 } }

    // Saves the state of the selected buttons
    val firstButtonSelected = remember { mutableStateOf(-1) }
    val secondButtonSelected = remember { mutableStateOf(-1) }
    // Saves the state of the number of guesses made
    val counter = remember { mutableStateOf(0) }
    // Saves the state of the user's lives
    val usersLives = remember { mutableStateOf(user.lives) }
    // Saves the state of the animation colors for each button
    val animationColours = remember { MutableList(10) { mutableStateOf(0) } }

    TestsScaffold(
        navController = navController
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(0.2F),
                painter = painterResource(id = R.drawable.wave_tests),
                contentDescription = stringResource(R.string.wavey_test_image),
                contentScale = ContentScale.FillBounds
            )
            Surface(modifier = Modifier
                .weight(0.12F)
                .height(100.dp)
                .width(100.dp)) {

                Image(
                    modifier = Modifier
                        .fillMaxSize()
                    ,
                    painter = painterResource(id = R.drawable.heart),
                    contentDescription = stringResource(R.string.heart),
                    contentScale = ContentScale.FillBounds
                )
                Text(text = usersLives.value.toString(), textAlign = TextAlign.Center, fontSize = 40.sp, modifier = Modifier.padding(20.dp), fontWeight = FontWeight.Bold, color = MaterialTheme.colorScheme.secondary)
            }


            //creating all the buttons
            PairingButtons(
                modifier = Modifier.weight(0.58F),
                firstButtonSelected,
                secondButtonSelected,
                counter,
                MaterialTheme.colorScheme.primaryContainer,
                MaterialTheme.colorScheme.primary,
                buttonClickableList,
                shuffledWords.value.first,
                animationColours
            )

            ButtonBoldText(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(0.12F),
                text = stringResource(R.string.guess),
                onClick = {
                    // if 2 buttons have been selected
                    if(firstButtonSelected.value != -1 && secondButtonSelected.value != -1) {
                        // put the button values into separate variables, due to deselecting the buttons but also using the values for indexing the lists
                        val firstButtonValue = firstButtonSelected.value
                        val secondButtonValue = secondButtonSelected.value
                        //is user guess correct variable
                        var isCorrect = false
                        //coroutine for heavy computations and animations
                        CoroutineScope(Dispatchers.Main).launch {

                            //looping through all the userWords and checking against the selected pair of strings
                            shuffledWords.value.second.forEach { userWord ->
                                //if the selected pair is the same as a word pair from a UserWord then the guess was correct
                                if ((userWord.firstLanguageWord.lowercase() == shuffledWords.value.first[firstButtonValue].lowercase() && userWord.secondLanguageWord.lowercase() == shuffledWords.value.first[secondButtonValue].lowercase()) || (userWord.secondLanguageWord.lowercase() == shuffledWords.value.first[firstButtonValue].lowercase() && userWord.firstLanguageWord.lowercase() == shuffledWords.value.first[secondButtonValue].lowercase())) {
                                    isCorrect = true

                                }
                            }
                            //deselect buttons
                            firstButtonSelected.value = -1
                            secondButtonSelected.value = -1

                            //if was correct guess
                            if (isCorrect) {
                                //disable selected buttons
                                buttonClickableList[firstButtonValue] = 2
                                buttonClickableList[secondButtonValue] = 2

                                // play animations on the selected buttons
                                animateButtonPair(
                                    true,
                                    animationColours[firstButtonValue],
                                    animationColours[secondButtonValue]
                                )
                                user.answersCorrect += 1
                                //check for game win
                                if (user.answersCorrect == 5) {
                                    //updating user variables
                                    user.questionTotal = gameManager.getLastQuestionNumber(user.difficulty)
                                    user.elapsedTime = gameManager.getElapsedTime()
                                    //go to game end screen
                                    navController.navigate(Screen.GameEnd.route)
                                }

                            } else {
                                //changing state to 1 which doesn't get rid of text and shadows for the buttons but allows the animation to play
                                buttonClickableList[firstButtonValue] = 1
                                buttonClickableList[secondButtonValue] = 1
                                //play incorrect animations
                                animateButtonPair(
                                    false,
                                    animationColours[firstButtonValue],
                                    animationColours[secondButtonValue]
                                )
                                // changing the buttons states back to 0 so the normal select/deselect colour changes happen
                                buttonClickableList[firstButtonValue] = 0
                                buttonClickableList[secondButtonValue] = 0
                                // updating user lives
                                usersLives.value -= 1
                                user.lives -= 1
                                //check for game loss
                                if (user.lives == 0) {
                                    //update user variables
                                    user.questionTotal = gameManager.getLastQuestionNumber(user.difficulty)
                                    user.elapsedTime = gameManager.getElapsedTime()
                                    //go to end screen
                                    navController.navigate(Screen.GameEnd.route)
                                }
                            }

                        }
                    }
                }
            )
        }

    }
}