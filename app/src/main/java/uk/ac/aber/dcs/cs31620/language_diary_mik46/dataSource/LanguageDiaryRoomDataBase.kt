package uk.ac.aber.dcs.cs31620.language_diary_mik46.dataSource

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

import androidx.sqlite.db.SupportSQLiteDatabase

import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordDao



@Database(entities = [UserWord::class], version = 1)
abstract class LanguageDiaryRoomDataBase : RoomDatabase() {

    abstract fun userWordDao(): UserWordDao

    companion object {
        private var instance: LanguageDiaryRoomDataBase? = null

        @Synchronized
        fun getDatabase(context: Context): LanguageDiaryRoomDataBase? {
            if (instance == null) {
                instance =
                    Room.databaseBuilder<LanguageDiaryRoomDataBase>(
                        context.applicationContext,
                        LanguageDiaryRoomDataBase::class.java,
                        "LanguageDiary_DataBase"
                    )
                        .allowMainThreadQueries()
                        .addCallback(roomDatabaseCallback(context))
                        .build()
            }
            return instance
        }

        private fun roomDatabaseCallback(context: Context): Callback {
            return object : Callback() {
            }
        }
    }
}