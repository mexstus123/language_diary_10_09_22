package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import androidx.compose.ui.graphics.vector.ImageVector

/**
 * Represents a group of buttons with an associated icon and onClick action.
 *
 * @param icon The icon to display with the button.
 * @param buttonText The text to display on the button.
 * @param message The message to display when the button is clicked.
 * @param onClick The action to perform when the button is clicked.
 */
data class OnClickButtonGroup(
    val icon: ImageVector,
    val buttonText: String,
    val message: String = "",
    val onClick: () -> Unit,
)
