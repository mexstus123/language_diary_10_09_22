package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers

import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord

/**
 * Manages the spelling game type using a list of words
 * which is passed to the super class GameManager.
 *
 * @param words The list of words to use in the game passed to GameManager.
 */
class SpellingGameManager(words: List<UserWord> ): GameManager(words) {

    /**
     * Gets a new word pair for the game from the wordsList.
     *
     * @param questionNumber The current question number.
     * @return A pair of words, where the first element is the word to be spelled and the second element is the word in the other language.
     */
    fun getNewWordPair(questionNumber: Int): Pair<String,String>{
        // getting UserWord from words list based on the current question number
        val currentWord = words[questionNumber]

        // Randomize whether the first or second language word should be spelled
        if(randomizeIsFirstLanguage()){
            return Pair(currentWord.firstLanguageWord,currentWord.secondLanguageWord)
        }
        return Pair(currentWord.secondLanguageWord,currentWord.firstLanguageWord)
    }
}