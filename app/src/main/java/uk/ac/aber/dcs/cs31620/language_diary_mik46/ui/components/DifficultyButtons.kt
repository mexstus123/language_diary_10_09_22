package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.AbsoluteRoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import uk.ac.aber.dcs.cs31620.language_diary_mik46.R
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.Difficulty


/**
 * A composable function that displays the three buttons on the tests screen
 * which select the difficulty of a tests.
 *
 * @param difficulty a [MutableState] holding the current [Difficulty] of the game. When a button is clicked, the value in this state is updated to the corresponding difficulty.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DifficultyButtons(difficulty: MutableState<Difficulty>){

    // reset the colour for all buttons
    var buttonOne = MaterialTheme.colorScheme.surfaceVariant
    var buttonTwo = MaterialTheme.colorScheme.surfaceVariant
    var buttonThree = MaterialTheme.colorScheme.surfaceVariant

    //change the buttons colour based on difficulty
    when(difficulty.value){
        Difficulty.EASY -> {buttonOne = MaterialTheme.colorScheme.tertiary}
        Difficulty.MEDIUM -> {buttonTwo = MaterialTheme.colorScheme.tertiary}
        Difficulty.HARD -> {buttonThree = MaterialTheme.colorScheme.tertiary}
    }
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {

        Spacer(modifier = Modifier.weight(0.18F))

        //easy button
        Surface(
            modifier = Modifier
                .weight(0.2F)
                .size(40.dp),
            color = buttonOne,
            border = BorderStroke(1.dp, MaterialTheme.colorScheme.secondary),
            shape = AbsoluteRoundedCornerShape(
                topLeft = 16.dp,
                topRight = 0.dp,
                bottomLeft = 16.dp,
                bottomRight = 0.dp
            ),
            shadowElevation = when(difficulty.value) {
                Difficulty.EASY ->{0.dp}
                Difficulty.MEDIUM ->{6.dp}
                Difficulty.HARD ->{6.dp}
                },
                onClick = { difficulty.value = Difficulty.EASY }) {
                    Column(verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
                        Text(
                            modifier = Modifier,
                            text = stringResource(R.string.easy),
                            fontWeight = FontWeight.Bold
                        )
                    }

                }

        //Medium button
        Surface(
            modifier = Modifier
                .weight(0.24F)
                .size(40.dp),
            color = buttonTwo,
            border = BorderStroke(1.dp, MaterialTheme.colorScheme.secondary),
            shape = AbsoluteRoundedCornerShape(
                topLeft = 0.dp,
                topRight = 0.dp,
                bottomLeft = 0.dp,
                bottomRight = 0.dp
            ),
            shadowElevation = when(difficulty.value) {
                Difficulty.EASY ->{6.dp}
                Difficulty.MEDIUM ->{0.dp}
                Difficulty.HARD ->{6.dp}
            },
            onClick = { difficulty.value = Difficulty.MEDIUM }) {
            Column(verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
                Text(
                    modifier = Modifier,
                    text = stringResource(R.string.medium),
                    fontWeight = FontWeight.Bold
                )
            }
        }
        //Hard button
        Surface(
            modifier = Modifier
                .weight(0.2F)
                .size(40.dp),
            color = buttonThree,
            border = BorderStroke(1.dp, MaterialTheme.colorScheme.secondary),
            shape = AbsoluteRoundedCornerShape(
                topLeft = 0.dp,
                topRight = 16.dp,
                bottomLeft = 0.dp,
                bottomRight = 16.dp
            ),
            shadowElevation = when(difficulty.value) {
                Difficulty.EASY ->{6.dp}
                Difficulty.MEDIUM ->{6.dp}
                Difficulty.HARD ->{0.dp}
            },
            onClick = { difficulty.value = Difficulty.HARD}) {
            Column(verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
                Text(
                    modifier = Modifier,
                    text = stringResource(R.string.hard),
                    fontWeight = FontWeight.Bold
                )
            }
        }
        Spacer(modifier = Modifier.weight(0.18F))
    }
}