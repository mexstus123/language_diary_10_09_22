package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers

import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord

/**
 * Manages the multiple choice game using a list of words.
 *
 * @param words The list of words which is passed to the super class.
 */
class MultipleChoiceGameManager(words: List<UserWord>): GameManager(words) {


    /**
     * Gets a list of words for the game with a correct answer.
     * //TODO This method could be optimized
     *
     * @return A pair of lists, where the first element is a list of word choices and the second element is the correct word.
     */
    fun getChoiceWordList(): Pair<List<String>, UserWord> {
        // using lazy to keep the variable after first initialization and not recompile the method on each call
        val currentQuestionWord: UserWord by lazy { pickRandomWordPair() }
        val wordChoiceList: MutableList<String> by lazy { emptyList<String>().toMutableList() }
        val isFirstLang: Boolean by lazy { randomizeIsFirstLanguage() }

        val mixedUserWord: UserWord by lazy {
            UserWord(
                currentQuestionWord.firstLanguageWord,
                currentQuestionWord.secondLanguageWord
            )
        }

        // Add the correct word to the list based on the randomized language
        if (isFirstLang) {
            wordChoiceList.add(0, currentQuestionWord.firstLanguageWord)
            //making sure the word to guess is always the first string in UserWord so i will know if the user guess is correct
            mixedUserWord.firstLanguageWord = currentQuestionWord.secondLanguageWord
            mixedUserWord.secondLanguageWord = currentQuestionWord.firstLanguageWord
        } else {
            //if word to guess is not going to be the user First Language add second language
            wordChoiceList.add(0, currentQuestionWord.secondLanguageWord)
        }
        //adding the other 3 words to the word choice list - the total is now 4
        for (i in 1..3) {
            //using getNoneIdenticalWord() here to make sure none of the words in the choice list are the same
            wordChoiceList.add(i, getNoneIdenticalWord(isFirstLang, wordChoiceList))
        }
        //randomize choice list order
        wordChoiceList.shuffle(secureRandom)
        //returning the choice list and the correct answer as pair
        return Pair(wordChoiceList, mixedUserWord)
    }

    /**
     * Gets a word that is not already in the current list.
     *
     * @param isFirstLang Whether the users first language word should be added.
     * @param currentWordsList The current list of choice words.
     * @return A word that is not already in the list.
     */
    private fun getNoneIdenticalWord(isFirstLang: Boolean, currentWordsList: List<String>): String {
        //getting random word
        var newRandomWord = pickRandomWordPair()
        var isAlreadyInList = true
        while (isAlreadyInList) {
            isAlreadyInList = false

            // Check if the word is already in the list based on the isFirstLang bool
            if (isFirstLang) {
                currentWordsList.forEach { word ->
                    if (word == newRandomWord.firstLanguageWord) {
                        isAlreadyInList = true
                    }
                }
            } else {
                currentWordsList.forEach { word ->
                    if (word == newRandomWord.secondLanguageWord) {
                        isAlreadyInList = true
                    }
                }
            }
            //if word is already in list, get a new random word and repeat the while loop
            if (isAlreadyInList) {
                newRandomWord = pickRandomWordPair()
            }
        }
        if (isFirstLang) {
            return newRandomWord.firstLanguageWord
        }
        return newRandomWord.secondLanguageWord
    }
}