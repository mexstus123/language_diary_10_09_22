package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * Data Access Object for the UserWord data class.
 * Contains methods for inserting, updating, deleting, and getting UserWord objects from the database.
 * [insertSingleWord] Inserts a single UserWord object into the database.
 * [updateWord] Updates a UserWord object in the database.
 * [deleteWord] Deletes a UserWord object from the database.
 * [deleteAll] Deletes all UserWord objects from the database.
 * [getAllWords] Gets all UserWord objects from the database as a LiveData object.
 * [getFavourites] Gets all UserWord objects marked as favorites from the database as a LiveData object.
 * [getNoFavourites] Gets all UserWord objects not marked as favorites from the database as a LiveData object.
 */
@Dao
interface UserWordDao {
    @Insert
    suspend fun insertSingleWord(userWord: UserWord)

    @Update
    suspend fun updateWord(userWord: UserWord)

    @Delete
    suspend fun deleteWord(userWord: UserWord)

    @Query("DELETE FROM UsersWords")
    suspend fun deleteAll()

    @Query("SELECT * FROM UsersWords")
    fun getAllWords(): LiveData<List<UserWord>>

    @Query("SELECT * FROM usersWords WHERE isFavourite = 1 ")
    fun getFavourites(): LiveData<List<UserWord>>

    @Query("SELECT * FROM usersWords WHERE isFavourite = 0 ")
    fun getNoFavourites(): LiveData<List<UserWord>>
}