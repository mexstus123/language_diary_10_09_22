package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

enum class AnimationColourValue(val animationValue: Int) { //TODO put instead of values in animations
    CONTAINER(0),
    INCORRECT(1),
    CORRECT(2),
    BACKGROUND(3),
    PRIMARY(4)
}
