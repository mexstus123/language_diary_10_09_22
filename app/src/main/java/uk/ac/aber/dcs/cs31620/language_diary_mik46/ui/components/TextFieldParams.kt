package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.activity.ComponentActivity
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import uk.ac.aber.dcs.cs31620.language_diary_mik46.MainActivity

/**
 * A composable function that displays an outlined text field with custom parameters.
 *
 * @param modifier A modifier to apply to the text field.
 * @param textValue The initial value of the text field.
 * @param onValueChange A callback to be invoked when the text field value changes.
 * @param startingText The text to display as the label for the text field.
 */
@Composable
fun  OutlinedTextFieldParams(
    modifier: Modifier = Modifier,
    textValue: String = "",
    onValueChange: (String) -> Unit = {},
    startingText: String,
) {
    OutlinedTextField(
        value = textValue,
        label = {
                Text(text = startingText)
        },
        onValueChange = onValueChange,
        modifier = modifier,
        keyboardOptions = KeyboardOptions(KeyboardCapitalization.Sentences),
    )
}

/**
 * A function that updates the value of a mutable state<String> object based on user input in a text field.
 *
 * @param newWord The new value entered by the user in the text field.
 * @param textFieldWord The text to update.
 * @param activity The activity in which the text field is displayed.
 * @param charLimit The maximum number of characters allowed in the text field.
 */
fun textFieldParameters(newWord:String, textFieldWord: MutableState<String>, activity: ComponentActivity, charLimit: Int = 14) {
    //if user tries to input 2 spaces in a row or tries to input a space when the text field is empty
    if(newWord.contains("  ") || (newWord == " " && textFieldWord.value.isEmpty())){
        //Do nothing
    }
    else if(newWord.contains('\n')) {
        //if user presses enter key - hide keyboard
        activity.currentFocus?.hideKeyboard()
    }
    else if(newWord.length <= charLimit){
        //if new word is not larger than 14 characters - update word
        textFieldWord.value = newWord
    }
}

/**
 * A function that hides the keyboard from the screen.
 */
fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}
/**
 * A composable function that displays a text field with custom parameters.
 *
 * @param modifier A modifier to apply to the text field.
 * @param textValue The initial value of the text field.
 * @param onValueChange A callback to be invoked when the text field value changes.
 * @param label The text to display as the label for the text field.
 * @param fontSize The font size to use for the label text.
 */
@Composable
fun TextFieldParams(
    modifier: Modifier = Modifier,
    textValue: String = "",
    onValueChange: (String) -> Unit = {},
    label: String,
    fontSize: Int = 20
) {
    TextField(
        value = textValue,
        label = {Text(text = label, fontSize = fontSize.sp) },
        onValueChange = onValueChange,
        modifier = modifier.height(50.dp),
    )
}