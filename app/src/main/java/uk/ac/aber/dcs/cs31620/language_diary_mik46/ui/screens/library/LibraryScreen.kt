package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.library

import androidx.activity.ComponentActivity
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.*
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.authentication_prototype.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.*

import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.User
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel

/**
 * A composable function that creates the screen for the user's library of words.
 *
 * @param navController the navigation controller for this screen
 * @param activity the activity this screen is being displayed in
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 */
@Composable
fun LibraryScreen(navController: NavHostController, activity: ComponentActivity, user: User, userWordsViewModel: UserWordsViewModel = viewModel()) {

    //setting variables
    val coroutineScope = rememberCoroutineScope()
    val searchFavouritesState = rememberSaveable { mutableStateOf(0) }
    val wordsList by when (searchFavouritesState.value) {
        0 -> {
            userWordsViewModel.wordsList.observeAsState(listOf())
        }
        1 -> {
            userWordsViewModel.getFavourites().observeAsState(listOf())
        }
        else -> {
            userWordsViewModel.getNoFavourites().observeAsState(listOf())
        }
    }
    val showDialog = rememberSaveable { mutableStateOf(false) }
    val wordToDeletePair = rememberSaveable { mutableStateOf(Pair("","")) }

    val searchLanguageState = rememberSaveable { mutableStateOf(0) }
    val searchWord = remember { mutableStateOf("") }
    MainScaffold(
        activity,
        navController = navController,
        userWordsViewModel,
        coroutineScope = coroutineScope
    ) { innerPadding ->

        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            //if delete button has been pressed show alertDialog for the warning before deleting a word
            if (showDialog.value) {
                AlertDialog(

                    title = { Text(text = stringResource(R.string.delete)) },
                    text = {
                        //this column is only due to the text layout bugging whiles just in one Text()
                        Column(
                            modifier = Modifier.width(230.dp),
                            horizontalAlignment = Alignment.CenterHorizontally,
                        ) {
                            Text(
                                text = "Word To Delete:",
                                textAlign = TextAlign.Center
                            )
                            Text(
                                text = "${user.firstLanguage}: ${wordToDeletePair.value.first}",
                                textAlign = TextAlign.Center
                            )
                            Text(
                                text = "${user.secondLanguage}: ${wordToDeletePair.value.second}",
                                textAlign = TextAlign.Center
                            )
                            Text(
                                text = "Do you still wish to delete?",
                                textAlign = TextAlign.Center
                            )
                        }
                    },
                    onDismissRequest = { showDialog.value = false },
                    icon = {
                        Icon(
                            imageVector = Icons.Filled.DeleteForever,
                            contentDescription = stringResource(R.string.delete)
                        )
                    },
                    dismissButton = {
                        Button(
                            onClick = { showDialog.value = false }) {
                            Text(stringResource(R.string.cancel))
                        }
                    },
                    confirmButton = {
                        Button(
                            onClick = {
                                showDialog.value = false
                                //database coroutine to delete the word from the database and wordlist stored in user
                                CoroutineScope(Dispatchers.IO).launch {
                                    wordsList.forEach { userWord ->
                                        if(userWord.firstLanguageWord == wordToDeletePair.value.first){
                                            userWordsViewModel.deleteWordFromDatabase(userWord)
                                            user.words.toMutableList().remove(userWord)

                                        }
                                    }

                                }

                            }) {
                            Text(stringResource(R.string.delete))
                        }
                    }
                )
            }
            Column(
                modifier = Modifier.fillMaxSize()
            ) {
                Surface(
                    modifier = Modifier
                        .height(120.dp)
                        .fillMaxWidth(),
                    color = MaterialTheme.colorScheme.primary,
                    tonalElevation = 10.dp,
                    shadowElevation = 6.dp
                ) {

                    Column(modifier = Modifier.fillMaxSize()) {
                        //search bar
                        Surface(
                            modifier = Modifier
                                .padding(20.dp, 0.dp, 20.dp, 4.dp)
                                .height(60.dp),
                            color = MaterialTheme.colorScheme.outline,
                            shape = RoundedCornerShape(30.dp)
                        ) {
                            Row(modifier = Modifier.fillMaxSize()) {
                                Icon(
                                    imageVector = Icons.Filled.Search,
                                    //tint = MaterialTheme.colorScheme.onBackground,
                                    contentDescription = stringResource(R.string.search_icon),
                                    modifier = Modifier
                                        .size(22.dp)
                                        .weight(0.1F)
                                        .align(Alignment.CenterVertically)
                                )


                                TextFieldParams(
                                    modifier = Modifier
                                        .fillMaxSize()
                                        .weight(0.9F),
                                    textValue = searchWord.value,
                                    onValueChange = {
                                        textFieldParameters(it, searchWord, activity)
                                    },
                                    label = stringResource(R.string.search),
                                    fontSize = 15
                                )

                            }
                        }
                        // row for the 2 drop down menus
                        Row(
                            modifier = Modifier
                                .weight(0.5F)
                                .fillMaxSize(),
                            horizontalArrangement = Arrangement.Center,
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            DropDownFromButton(
                                items = listOf<String>(
                                    stringResource(R.string.all_languages),
                                    user.firstLanguage,
                                    user.secondLanguage
                                ),
                                modifier = Modifier
                                    .height(50.dp)
                                    .fillMaxWidth()
                                    .weight(0.5F)
                                    .padding(start = 8.dp, top = 4.dp, end = 8.dp),
                                itemClick = {
                                    //on itemClick changing the language state
                                    when (it) {
                                        "All Languages" -> {
                                            searchLanguageState.value = 0
                                        }
                                        user.firstLanguage -> {
                                            searchLanguageState.value = 1
                                        }
                                        user.secondLanguage -> {
                                            searchLanguageState.value = 2
                                        }
                                    }
                                }
                            )

                            DropDownFromButton(
                                items = listOf<String>("All Words", "Favourites", "No Favourites"),
                                modifier = Modifier
                                    .height(50.dp)
                                    .fillMaxWidth()
                                    .weight(0.5F)
                                    .padding(start = 8.dp, top = 4.dp, end = 8.dp),
                                itemClick = {
                                    //on itemClick changing the search state
                                    when (it) {
                                        "All Words" -> {
                                            searchFavouritesState.value = 0
                                        }
                                        "Favourites" -> {
                                            searchFavouritesState.value = 1
                                        }
                                        "No Favourites" -> {
                                            searchFavouritesState.value = 2
                                        }
                                    }
                                }
                            )

                        }
                    }
                }
                Column(
                    modifier = Modifier
                        .weight(0.85F)
                        .fillMaxSize()
                        .verticalScroll(rememberScrollState())
                ) {
                    //add each word to the screen with icons next to them
                    if (wordsList.isNotEmpty()) {
                        LibraryColumn(wordsList, showDialog, wordToDeletePair, searchFavouritesState.value,searchLanguageState.value,userWordsViewModel,searchWord.value)
                    }
                }
            }

        }

    }
}





