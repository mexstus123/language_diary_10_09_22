package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import uk.ac.aber.dcs.cs31620.language_diary_mik46.R


/**
 * A composable function that displays a "top app" which is just a FAB in top left corner.
 *
 * @param onClick A callback to be invoked when the floating action button is clicked.
 */
@Composable
fun TestScreensTopAppBar(
    onClick: () -> Unit = {}
){
    FloatingActionButton(
        modifier = Modifier.size(82.dp).padding(14.dp),
        containerColor = MaterialTheme.colorScheme.primary,
            onClick = onClick) {
                Icon(
                    imageVector = Icons.Filled.ArrowBack,
                    contentDescription = stringResource(R.string.go_back_arrow),
                    tint = MaterialTheme.colorScheme.onPrimary
                )
    }
}
