package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.game_end

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.GameEndInfoSurface
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.TestsScaffold
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.User
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.navigation.Screen
import uk.ac.aber.dcs.cs31620.language_diary_mik46.R

/**
 * A composable function that creates the end screen for every test
 * using all the data stored in the user class.
 *
 * @param navController the navigation controller for this screen
 * @param user the user object
 */
@Composable
fun GameEndScreen(navController: NavHostController, user: User) {

    val testPassed = rememberSaveable{mutableStateOf(user.lives != 0 && user.answersCorrect > (user.questionTotal * 0.4).toInt())}

    TestsScaffold(
        navController = navController,
    ) {
        BackHandler(enabled = true, onBack = {navController.popBackStack(Screen.Tests.route, false)})

        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(150.dp),
                painter = painterResource(R.drawable.wave_sidetoside),
                contentDescription = stringResource(R.string.wave_across),
                contentScale = ContentScale.FillBounds
            )
            Text(
                modifier = Modifier.height(60.dp),
                fontSize = 40.sp,
                text = when(testPassed.value){
                true -> {
                    stringResource(R.string.test_passed)
                }
                false -> {
                    stringResource(R.string.test_failed)
                }
            })

            GameEndInfoSurface(
                modifier = Modifier,
                title = stringResource(R.string.questions_correct),
                info = "${user.answersCorrect} / ${user.questionTotal}"
            )

            GameEndInfoSurface(
                modifier = Modifier,
                title = stringResource(R.string.time_taken),
                info = user.elapsedTime
            )

            GameEndInfoSurface(
                modifier = Modifier,
                title = stringResource(R.string.difficulty),
                info = "${user.difficulty}"
            )

            Image(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(150.dp),
                painter = painterResource(R.drawable.wave_sidetoside),
                contentDescription = stringResource(R.string.wave_across),
                contentScale = ContentScale.FillBounds
            )
        }
    }
}