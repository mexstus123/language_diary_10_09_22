package uk.ac.aber.dcs.cs31620.language_diary_mik46.file_manager

import android.util.Log
import androidx.activity.ComponentActivity
import uk.ac.aber.dcs.cs31620.language_diary_mik46.MainActivity
import uk.ac.aber.dcs.cs31620.language_diary_mik46.TAG
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.User
import java.io.File
import java.nio.file.Files.exists


fun createLanguageFile(activity: ComponentActivity, user: User){
    val languageFile = File(activity.filesDir, "Languages.txt")

    languageFile.writeText("${user.firstLanguage}\n${user.secondLanguage}")
    Log.i(TAG,"Created new ${activity.filesDir}/Languages.txt")
}

fun readLanguageFile(activity: ComponentActivity): Pair<String,String>{
    val languageFile = File(activity.filesDir, "Languages.txt")


    val languagesFromFile = languageFile.readText()
    var firstLanguage = ""
    var secondLanguage = ""
    var isFirst = true

    languagesFromFile.forEach { char ->
        if (char == '\n'){
            isFirst = false
        }else if (isFirst){
            firstLanguage += char
        }else{
            secondLanguage += char
        }
    }
    return Pair(first = firstLanguage, second = secondLanguage)
}

fun deleteLanguageFile(activity: ComponentActivity){
    try {
        val languageFile = File(activity.filesDir, "Languages.txt")
        if (exists(languageFile.toPath())) {
            languageFile.delete()
        }
    }
    catch (_: SecurityException){

    }
}