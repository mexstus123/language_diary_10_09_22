package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.test_screens

import androidx.activity.ComponentActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import kotlinx.coroutines.*
import uk.ac.aber.dcs.cs31620.language_diary_mik46.MainActivity
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.*
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.*
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.SpellingGameManager
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.navigation.Screen
import uk.ac.aber.dcs.cs31620.language_diary_mik46.R

/**
 * A composable function that displays the spelling test screen for the user to complete.
 * The function displays a random translation of a word in the users library and the user must enter the translation.
 * @param navController: the navigation controller to be used for navigating to different screens
 * @param activity: the current activity
 * @param user: the current user taking the test
 */
@Composable
fun SpellingTestScreen(navController: NavHostController, activity: ComponentActivity, user: User) {


// Creates a game manager with the list of word pairs the user is being tested on
    val gameManager = remember { SpellingGameManager(user.randomizedQuestions) }
// Keeps track of the current question number
    val questionNumber = remember { mutableStateOf(0) }
// The current word pair the user is being tested on
    val wordPairToGuess = remember { mutableStateOf(gameManager.getNewWordPair(questionNumber.value)) }

// Keeps track of the current color of the primary container surface
    val colourSelector = remember { mutableStateOf(0) }

// Keeps track of the number of answers the user has gotten correct so far
    val answersCorrect = remember { mutableStateOf(0) }

    // The main scaffold for the test screen
    TestsScaffold(
        navController = navController
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(0.26F),
                painter = painterResource(id = R.drawable.wave_tests),
                contentDescription = stringResource(R.string.wavey_test_image),
                contentScale = ContentScale.FillBounds
            )
            Surface(
                modifier = Modifier
                    .weight(0.2F)
                    .fillMaxSize()
                    .padding(20.dp),
                color = FadeAnimationColours(colourSelector.value),
                shape = RoundedCornerShape(20.dp)
            ) {
                Column(verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
                    Text(
                        text = wordPairToGuess.value.first,
                        color = MaterialTheme.colorScheme.secondary,
                        fontSize = 40.sp)//TODO add list of randomized words
                }

            }


            Spacer(modifier = Modifier.weight(0.03F))

                val usersGuess = rememberSaveable { mutableStateOf("") }
                OutlinedTextFieldParams(
                    modifier = Modifier
                        .width(250.dp)
                        .height(65.dp),
                    textValue = usersGuess.value,
                    onValueChange = {
                        textFieldParameters(it, usersGuess, activity)
                    },
                    startingText = stringResource(id = R.string.translate),
                )



            ButtonBoldText(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(0.13F),
                text = stringResource(R.string.next_question),
                onClick = {
                    //coroutine for animations
                    CoroutineScope(Dispatchers.Main).launch {
                        //if users guess is correct
                        if (usersGuess.value.lowercase() == wordPairToGuess.value.second.lowercase()) {
                            //update correct answers and play correct animation
                            answersCorrect.value += 1
                            animatePrimaryContainerColour(true, colourSelector)
                        } else {
                            //if user guess is wrong play incorrect animations
                                animatePrimaryContainerColour(false, colourSelector)
                        }

                        //checking for game end using the last question number
                        if (questionNumber.value == gameManager.getLastQuestionNumber(user.difficulty) - 1) {
                            //update user variables to be used in end screen
                            user.questionTotal = gameManager.getLastQuestionNumber(user.difficulty)
                            user.answersCorrect = answersCorrect.value
                            user.elapsedTime = gameManager.getElapsedTime()
                            // go to end screen
                            navController.navigate(Screen.GameEnd.route)
                        } else {
                            //if not game end update question number and get next word to guess
                            questionNumber.value += 1
                            wordPairToGuess.value = gameManager.getNewWordPair(questionNumber.value)
                            usersGuess.value = ""
                        }
                    }
                }
            )
            Spacer(modifier = Modifier.weight(0.3F))
        }

    }
}



