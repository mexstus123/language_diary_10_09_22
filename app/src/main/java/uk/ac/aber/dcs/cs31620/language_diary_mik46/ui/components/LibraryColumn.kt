package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DeleteForever
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.material3.*
import androidx.compose.runtime.*

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel

/**
 * Composable function that creates a column of words in the library screen.
 * The words are filtered based on the specified search text and selected items from the drop downs.
 * Each row contains the [UserWord] and then icon buttons to favourite or remove it from db
 *
 * @param wordsList the list of words to display in the column
 * @param showDialog a [MutableState] object that controls the visibility of the delete alertDialog
 * @param wordToDelete a [MutableState] object that holds the index of the word to delete in the list
 * @param searchFavouritesState an integer representing the state of the favourites search filter (0: all, 1: favourites, 2: non-favourites)
 * @param searchLanguageState an integer representing the state of the language search filter (0: both languages, 1: first language, 2: second language)
 * @param userWordsViewModel the [UserWordsViewModel] to update words in the list after change
 * @param searchText the search text to filter the list of words
 */
@Composable
fun LibraryColumn(
    wordsList: List<UserWord>,
    showDialog: MutableState<Boolean>,
    wordToDelete: MutableState<Pair<String,String>>,
    searchFavouritesState: Int,
    searchLanguageState: Int,
    userWordsViewModel: UserWordsViewModel,
    searchText: String

){
    // Create a list of mutable states to keep track of whether each slide menu is open
    val isOpenStateList = remember { MutableList(wordsList.size) { mutableStateOf(false) } }
    
    // For each word in the list
    wordsList.forEachIndexed { index, item ->
        // Filter the word based on the search text and language state
        if (when (searchLanguageState) {
                0 -> {
                    item.firstLanguageWord.lowercase().contains(searchText.lowercase()) || item.secondLanguageWord.lowercase().contains(searchText.lowercase())
                }
                1 -> {
                    item.firstLanguageWord.lowercase().contains(searchText.lowercase())
                }
                else -> {
                    item.secondLanguageWord.lowercase().contains(searchText.lowercase())
                }
            }
        ) {
            //extra isfav variable to make it update on press
            var isfav by mutableStateOf(item.isFavourite)

            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(80.dp)
                    .padding(5.dp, 2.dp, 0.dp, 5.dp)
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(0.52F),
                    horizontalAlignment = Alignment.Start
                ) {
                    Text(
                        //hiding first lang based on searchLanguageState
                        text = when (searchLanguageState) {
                            2 -> {
                                ""
                            }
                            else -> {
                                item.firstLanguageWord
                            }
                        },
                        color = MaterialTheme.colorScheme.primary,
                        textAlign = TextAlign.Start,
                        fontSize = 30.sp
                    )
                    Text(
                        //hiding second lang based on searchLanguageState
                        text = when (searchLanguageState) {
                            1 -> {
                                ""
                            }
                            else -> {
                                item.secondLanguageWord
                            }
                        },
                        color = MaterialTheme.colorScheme.onPrimaryContainer,
                        textAlign = TextAlign.Start,
                        fontSize = 20.sp
                    )
                }
                //this spacer is what moves the icons across
                if (!isOpenStateList[index].value) {
                    Spacer(modifier = Modifier.weight(0.18F))
                }

                IconButton(
                    onClick = {
                        //when icon is pressed open or close the menu
                        isOpenStateList[index].value = !isOpenStateList[index].value
                              },
                ) {
                    Icon(
                        imageVector = Icons.Default.MoreVert,
                        tint = MaterialTheme.colorScheme.onBackground,
                        contentDescription = item.firstLanguageWord+ " menu",
                        modifier = Modifier
                            .fillMaxSize()
                            .weight(0.1F)
                            .align(Alignment.CenterVertically)
                    )
                }

                    // if the menu is open for this item create the favourite and delete iconButtons
                if (isOpenStateList[index].value) {
                    IconButton(
                        onClick = {
                            //on click favourite or un favourite the item
                            item.isFavourite = !item.isFavourite
                            //this stops the item sliders states being passed to other items
                            if (searchFavouritesState != 0) {
                                isOpenStateList[index].value = !isOpenStateList[index].value
                            }
                            //update the db
                            userWordsViewModel.updateWord(item)
                            //this var updates the ui immediately on tap
                            isfav = item.isFavourite
                        },
                    ) {
                        Icon(
                            //update the icon based on isfav var
                            imageVector = when (isfav) {
                                true -> {
                                    Icons.Filled.Favorite
                                }
                                false -> {
                                    Icons.Outlined.FavoriteBorder
                                }
                            },
                            //update the icon colour on isfav var
                            tint = when (isfav) {
                                true -> {
                                    MaterialTheme.colorScheme.primary
                                }
                                false -> {
                                    MaterialTheme.colorScheme.onBackground
                                }
                            },
                            contentDescription = item.firstLanguageWord + " heart",
                            modifier = Modifier
                                .size(30.dp)
                                .weight(0.1F)
                                .align(Alignment.CenterVertically)
                        )
                    }
                    IconButton(
                        onClick = {
                            //making note of the item to delete
                            wordToDelete.value = Pair(wordsList[index].firstLanguageWord,wordsList[index].secondLanguageWord)
                            //setting showDialog to true so the alertDialog will pop up
                            showDialog.value = true
                            //close the menu so its not passed to the next item when deleted
                            isOpenStateList[index].value = false

                        },
                    ) {
                        Icon(
                            imageVector = Icons.Filled.DeleteForever,
                            tint = MaterialTheme.colorScheme.onBackground,
                            contentDescription = item.firstLanguageWord + " delete",
                            modifier = Modifier
                                .size(35.dp)
                                .weight(0.1F)
                                .align(Alignment.CenterVertically)
                        )
                    }
                }


            }
            Divider(
                modifier = Modifier.padding(20.dp, 10.dp, 20.dp, 0.dp),
                color = MaterialTheme.colorScheme.surfaceVariant
            )
        }
    }
}