package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model

import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.Difficulty
import java.security.SecureRandom

/**
 * Class representing the current user of the app.
 *
 * @param firstLanguage The user's first language choice - loaded from Languages file.
 *
 * @param secondLanguage The user's second language - loaded from Languages file.
 *
 * @param words The list of words known by the user - loaded from database.
 *
 * @param randomizedQuestions List of randomized questions for the games/tests.
 *
 * @param lives The number of lives the user has, stored here to pass to the end screen rather than passing a GameManager.
 *
 * @param answersCorrect The number of answers the user has gotten correct, stored here to pass to the end screen rather than passing a GameManager.
 *
 * @param difficulty The current difficulty level selected.
 *
 * @param elapsedTime The elapsed time of the game, stored here to pass to the end screen rather than passing a GameManager.
 *
 * @param questionTotal The total number of questions in the game, stored here to pass to the end screen rather than passing a GameManager.
 */
class User(
    val firstLanguage: String,
    val secondLanguage: String,
    var words: List<UserWord>,
    var randomizedQuestions: List<UserWord> = emptyList(),
    var lives: Int = 5,
    var answersCorrect: Int = 0,
    var difficulty: Difficulty = Difficulty.MEDIUM,
    var elapsedTime: String = "",
    var questionTotal: Int = 0
){

    /**
     * Creates a capped list of randomized questions for the user based on the difficulty level.
     * NOTE - if there is not enough the words in library randomizedQuestions will always
     *      - be empty. This is condition used for test screen buttons & to show snackbar.
     * @param listSize The needed size for the randomized list of questions.
     */
    fun createCappedRandomizedList(listSize: Int) {
        //reset the variables for each test
        randomizedQuestions = emptyList()
        var randomWordsList = emptyList<UserWord>()
        answersCorrect = 0

        //list size based on difficulty
        var newListSize = listSize
        val secureRandom = SecureRandom()
        //this just makes sure there is always at least 1 question due to HangMan game type always being size 1
        //check to see if theres enough words in the users library to take test
        if (isEnoughUserWords(listSize)) {
            if (words.size < 30) {
                randomWordsList = words.shuffled(secureRandom)
                randomizedQuestions = randomWordsList.take(newListSize)
            }else{
                randomWordsList = words.take(newListSize)
                randomizedQuestions = randomWordsList.shuffled(secureRandom)
            }
        }
    }

    /**
     * Creates an uncapped(Technically capped at 30 but will never use that many)
     * list of randomized questions for the quizzes.
     * I gave it cap of 30 due to runtime and games will never need more than this
     */
    fun createUnCappedRandomList(){
        //variable resets
        var randomWordsList = mutableListOf<UserWord>()
        val secureRandom = SecureRandom()
        answersCorrect = 0
        // making sure users library size is big enough for the MultipleChoice and Pairing game types
        if(words.size >= 5 ){
            //if user has less than 30 words just randomize the words List
            if (words.size < 30){
                randomWordsList = words.shuffled(secureRandom).toMutableList()
            }else{
                // randomly pick 30 words from word list
                for (i in 0..29){
                    val randomInt = secureRandom.nextInt((words.size - 1))
                    randomWordsList.add(words[randomInt])
                }
            }
            randomizedQuestions = randomWordsList
        }else{
            randomizedQuestions = emptyList()
        }
    }


    /**
     * Sets the number of lives for the current game to the difficulty level specified.
     */
    fun setLivesToDifficulty(){
        answersCorrect = 0
        lives = 5
        lives = (lives.toDouble() * difficulty.livesMultiplier).toInt()
    }


    /**
     * Returns true if there are enough words in the user's list to generate the required number of questions.
     *
     * @param listSizeNeeded: The number of questions needed.
     * @return True if there are enough words in the user's list to generate the required number of questions,
     * false otherwise.
     */
    private fun isEnoughUserWords(listSizeNeeded: Int): Boolean{
        if(words.size < (listSizeNeeded)){
            return false
        }
        return true
    }
}



