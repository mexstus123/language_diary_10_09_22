package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.navigation

/**
 * A sealed class representing the screens in the app.
 *
 * @property route a string representing the route for the screen
 */
sealed class Screen(
    val route: String
){
    /**
     * Each one of these objects represents a screen in the application.
     */
    object AddWord : Screen("AddWord")
    object Tests : Screen("Tests")
    object Library : Screen("Library")
    object SpellingTest : Screen("SpellingTests")
    object MultipleChoiceTest : Screen("MultipleChoiceTests")
    object HangManTest : Screen("HangManTests")
    object PairingTest : Screen("PairingTests")
    object GameEnd : Screen("GameEnd")

}
/**
 * A list of main screens in the app for the navigation bar.
 */
val mainScreens = listOf(
    Screen.AddWord,
    Screen.Tests,
    Screen.Library
)