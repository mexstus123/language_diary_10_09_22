package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarData
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier


/**
 * Composable function that creates a snackbar with default styling.
 *
 * @param data the [SnackbarData] object containing the message and action label to be displayed in the snackbar
 * @param modifier the [Modifier] for the snackbar
 * @param onDismiss lambda to be called when the snackbar is dismissed, either by the action or by timeout
 */
@Composable
fun DefaultSnackBar(
    data: SnackbarData,
    modifier: Modifier = Modifier,
    onDismiss: () -> Unit
){
    // Create a snackbar with the provided modifier and content
    Snackbar(
        modifier = modifier,
        content = {
            // Display the message from the SnackbarData object
            Text(text = data.visuals.message)
        },
        // If the SnackbarData object has an action label, create a TextButton to dismiss the snackbar on click
        action = {
            data.visuals.actionLabel?.let{ actionLabel -> 
                TextButton(onClick = { onDismiss() }
                ){
                    Text(text = actionLabel)
                } 
                    
            }
        }
    )
}