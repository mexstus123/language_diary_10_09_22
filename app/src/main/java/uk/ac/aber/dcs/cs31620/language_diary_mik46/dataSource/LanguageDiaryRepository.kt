package uk.ac.aber.dcs.cs31620.language_diary_mik46.dataSource

import android.app.Application
import androidx.lifecycle.LiveData
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord

/**
 * Repository class responsible for handling data operations.
 * This is the mediator between the [UserWordDao] and the [UserWordsViewModel].
 *
 * @param application The application context
 * @property userWordDao The Data Access Object for the [UserWord] class
 */
class LanguageDiaryRepository(application: Application) {
    /**
     * The UserWordDao for the repository.
     */
    private val userWordDao = LanguageDiaryRoomDataBase.getDatabase(application)!!.userWordDao()

    /**
     * Insert a single [UserWord] into the database
     * @param userWord The [UserWord] object to be inserted
     */
    suspend fun insert(userWord: UserWord) {
        userWordDao.insertSingleWord(userWord)
    }

    /**
     * Update a single [UserWord] object in the database
     * @param userWord The [UserWord] object to be updated
     */
    suspend fun updateWord(userWord: UserWord) {
        userWordDao.updateWord(userWord)
    }

    /**
     * Delete all objects in the database
     */
    suspend fun deleteAll() {
        userWordDao.deleteAll()
    }

    /**
     * Delete a single [UserWord] object in the database
     * @param userWord The [UserWord] object to be deleted
     */
    suspend fun deleteWord(userWord: UserWord) {
        userWordDao.deleteWord(userWord)
    }

    /**
     * Gets all objects in the database
     */
    fun getAllWords(): LiveData<List<UserWord>> {
        return userWordDao.getAllWords()
    }

    /**
     * Gets Favourites from the database
     */
    fun getFavouriteWords(): LiveData<List<UserWord>> {
        return userWordDao.getFavourites()
    }

    /**
     * Gets No Favourites from the database
     */
    fun getNoFavouriteWords(): LiveData<List<UserWord>> {
        return userWordDao.getNoFavourites()
    }
}