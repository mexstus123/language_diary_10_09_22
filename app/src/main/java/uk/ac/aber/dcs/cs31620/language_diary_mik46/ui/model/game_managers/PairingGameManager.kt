package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers

import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord

/**
 * Manages the pairing game using a list of words.
 *
 * @param words The list of words which is pass to super class.
 */
class PairingGameManager (words: List<UserWord>): GameManager(words) {

    /**
     * Gets a shuffled list of words for the game.
     *
     * @return A pair of lists, where the first element is a list of all words shuffled and the
     *         second element is a list of user words to use to compare the user guesses against
     */
    fun getShuffledWordList(): Pair<List<String>, List<UserWord>> {

        val userWordList = mutableListOf<UserWord>()
        val allWordsList = mutableListOf<String>()

        // Add the first five words to the lists
        for (i in 0..4){
            userWordList.add(words[i])
            allWordsList.add(words[i].firstLanguageWord)
            allWordsList.add(words[i].secondLanguageWord)
        }
        // Shuffle the all words list
        allWordsList.shuffle(secureRandom)
        return Pair(allWordsList, userWordList)
    }

    /**
     * Gets the last question number for the game.
     * using polymorphism to change the game manager getLastQuestionNumber()
     * @param difficulty The difficulty level of the game (only needed due to override).
     * @return The last question number.
     */
    override fun getLastQuestionNumber(difficulty: Difficulty): Int {
        //just returning 5 as the last question for pairing will always be 5
        return 5
        }
}


