package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable

import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import uk.ac.aber.dcs.cs31620.language_diary_mik46.MainActivity
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.navigation.Screen


/**
 * A composable function that displays a scaffold with custom parameters for testing purposes.
 *
 * @param navController The navigation controller for the app.
 * @param pageContent A composable function that displays the content of the page.
 * @param innerPadding The padding values to use for the page content.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TestsScaffold(
    navController: NavHostController,
    pageContent:
    @Composable (innerPadding: PaddingValues) -> Unit = {}
) {

    Scaffold(
        topBar = {
            TestScreensTopAppBar(onClick = {
                navController.popBackStack(Screen.Tests.route, false)
            }
            )
        },
        bottomBar = { },
        content = { innerPadding -> pageContent(innerPadding) }

        )
}
