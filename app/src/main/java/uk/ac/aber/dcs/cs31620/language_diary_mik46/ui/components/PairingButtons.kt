package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

/**
 * A composable function that displays a grid of buttons with custom colors, animations and onclick actions.
 * This is the most complicated algorithm in my project.
 * A loop is needed to iterate through all the buttons so that the index can be taken
 * for all the list.
 *
 * @param modifier A modifier to apply to the button grid.
 * @param firstButtonNumberPressed A mutable state object that stores the number of the first button pressed.
 * @param secondButtonNumberPressed A mutable state object that stores the number of the second button pressed.
 * @param counter A mutable state object that stores the number of buttons currently pressed.
 * @param unselectedColour The color to use for unselected buttons.
 * @param selectedColour The color to use for selected buttons.
 * @param unClickableButtonsList A list of integers that determines which buttons are clickable due to user getting correct answers.
 * @param wordsList A list of words to display on the buttons. 5 FirstLang words and 5 SecondLang words.
 * @param colourSelectorList A list of mutable state for the animations tp be played.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PairingButtons(modifier: Modifier,
                   firstButtonNumberPressed: MutableState<Int>,
                   secondButtonNumberPressed: MutableState<Int>,
                   counter: MutableState<Int>,
                   unselectedColour: Color,
                   selectedColour: Color,
                   unClickableButtonsList: MutableList<Int>,
                   wordsList: List<String>,
                   colourSelectorList: MutableList<MutableState<Int>>) {

    // Set all the buttons to unselectedColour
    val buttonColors = MutableList(10) { unselectedColour}

    // Change the color of the first selected buttons to the selected color
    if (firstButtonNumberPressed.value in 0..9) {
        buttonColors[firstButtonNumberPressed.value] = selectedColour
    }
    // Change the color of the second selected buttons to the selected color
    if (secondButtonNumberPressed.value in 0..9) {
        buttonColors[secondButtonNumberPressed.value] = selectedColour
    }

    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        // Iterate through the buttons in rows of 2
        for (i in 0 until 10 step 2) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(0.1F),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Surface(
                    modifier = Modifier
                        .weight(0.5F)
                        .fillMaxSize()
                        .padding(15.dp),
                    // Set the shadow elevation of the button based on whether it is clickable or not
                    shadowElevation = when(unClickableButtonsList[i]){
                        0 -> {6.dp}
                        1 -> {6.dp}
                        else ->{ 0.dp}},
                    // Set the colour of the button based on whether it is clickable or not
                    color = when(unClickableButtonsList[i]){
                        0 -> {buttonColors[i]}
                        //this is the animation
                        else ->{ FadeAnimationColours(colourSelectorList[i].value)}
                                                           },
                    // enable button when clickable
                    enabled = when(unClickableButtonsList[i]){
                        0 -> {true}
                        1 -> {true}
                        else ->{ false}
                        },
                    shape = RoundedCornerShape(10.dp),
                    onClick = {
                        pairingButtonsOnClick( firstButtonNumberPressed, secondButtonNumberPressed, counter, i)
                    }
                ) {
                    Column(
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            modifier = Modifier,
                            //hiding text when button is unClickable
                            text = when(unClickableButtonsList[i]){
                                0 -> {wordsList[i]}
                                1 -> {wordsList[i]}
                                else ->{ ""}},
                            fontWeight = FontWeight.Bold
                        )
                    }
                }
                //making sure it doesn't go out of bounds
                if (i + 1 < 10) {
                    //this is now the same as the above but with i + 1 as there are 2 buttons per row
                    Surface(
                        modifier = Modifier
                            .weight(0.5F)
                            .fillMaxSize()
                            .padding(15.dp),
                        shadowElevation = when(unClickableButtonsList[i + 1]){
                            0 -> {6.dp}
                            1 -> {6.dp}
                            else ->{ 0.dp}},
                        color = when(unClickableButtonsList[i+1]){
                            0 -> {buttonColors[i + 1]}
                            else ->{ FadeAnimationColours(colourSelectorList[i+1].value)}
                                                                 },
                        enabled = when(unClickableButtonsList[i + 1]){
                            0 -> {true}
                            1 -> {true}
                            else ->{ false}
                                                                     },
                        shape = RoundedCornerShape(10.dp),
                        onClick = {
                            pairingButtonsOnClick( firstButtonNumberPressed, secondButtonNumberPressed, counter, i+1)
                        }
                    ) {
                        Column(
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Text(
                                modifier = Modifier,
                                text = when(unClickableButtonsList[i + 1]){
                                    0 -> {wordsList[i + 1]}
                                    1 -> {wordsList[i + 1]}
                                    else ->{ ""}},
                                fontWeight = FontWeight.Bold
                            )
                        }
                    }

                }
            }

        }

    }
}

/**
 * Handles the logic for selecting and deselecting buttons in the PairingButtons composable.
 *
 * @param firstButtonNumberPressed A mutable state object that stores the number of the first button pressed.
 * @param secondButtonNumberPressed A mutable state object that stores the number of the second button pressed.
 * @param counter A mutable state object that stores the number of buttons currently pressed.
 * @param currentIndex index of the button pressed.
 */
fun pairingButtonsOnClick(
    firstButtonNumberPressed: MutableState<Int>,
    secondButtonNumberPressed: MutableState<Int>,
    counter: MutableState<Int>,
    currentIndex: Int)
{
    //if button pressed and this button is already selected as first
    if (firstButtonNumberPressed.value == currentIndex) {
        //deselect
        firstButtonNumberPressed.value = -1
        counter.value--
        //if button pressed and this button is already selected as second
    } else if (secondButtonNumberPressed.value == currentIndex) {
        //deselect
        secondButtonNumberPressed.value = -1
        counter.value--
        //if no buttons are selected
    } else if (counter.value == 0) {
        //select button as first
        firstButtonNumberPressed.value = currentIndex
        counter.value++
        //if one button is selected
    } else if (counter.value == 1) {
        //select button as second
        secondButtonNumberPressed.value = currentIndex
        counter.value++
        // if already 2 buttons selected
    } else if (counter.value == 2) {
        //swap the first and second - because it looks less blocky for the user
        firstButtonNumberPressed.value = secondButtonNumberPressed.value
        //this button replaces the second selection
        secondButtonNumberPressed.value = currentIndex
    }
}