package uk.ac.aber.dcs.cs31620.language_diary_mik46

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import uk.ac.aber.dcs.cs31620.language_diary_mik46.file_manager.createLanguageFile
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.add_word.AddWordScreen
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.User
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.library.LibraryScreen
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.navigation.Screen
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.pick_language.PickLanguageScreen
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.theme.LanguageDiaryTheme
import java.io.File
import java.nio.file.Files.exists
import java.nio.file.Paths
import uk.ac.aber.dcs.cs31620.language_diary_mik46.file_manager.readLanguageFile
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.game_end.GameEndScreen
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.test_screens.*


const val TAG = "FileCreation"
/**
 * The main activity of the app.
 * This activity sets the content to a surface container with a LanguageDiaryTheme,
 * and either creates a new user or retrieves an existing user depending on the presence of the language file.
 * When there is no language user is sent to the pick language screen where a language file is created and the activity is recreated.
 * When there is a language file the user is sent to the Add word screen through the navigation graph
 */
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //lock orientation
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        var isNewUser = false
        //checking for a language file
        if (!exists(Paths.get(File(filesDir, "Languages.txt").path))){
            // will assume user is a new user if there is no language file
            isNewUser = true
        }

        setContent {
            LanguageDiaryTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    //create empty user
                    //if language file does not exist, so if is a new user
                    if (isNewUser){
                        Log.i(TAG,"Created new ${filesDir}/Languages.txt")
                        //getting user from the start screen
                        PickLanguageScreen(activity = this)

                    }else{
                        //if not a new user read from language file
                        val languages = readLanguageFile(this)

                        // create user with languages from file and build the navigation graph
                        CreateUserPassToBuildNav(languages.first,languages.second, activity = this)
                    }

                }
            }
        }

    }
}

/**
 * Builds a navigation graph using Jetpack Compose's NavHost composable.
 * The navController is remembered so it can be used by other composables to navigate between screens.
 * The start destination is set to the route for the AddWordScreen.
 *
 * @param activity the MainActivity instance is passed for mainly text fields
 * @param user the current user is passed for data transfer between screens
 * @param userWordsViewModel the ViewModel for the user's words is passed for querying the database
 */
@Composable
fun BuildNavigationGraph(activity: ComponentActivity, user: User, userWordsViewModel: UserWordsViewModel = viewModel()){

    val navController = rememberNavController()


    NavHost(
        navController = navController,
        startDestination = Screen.AddWord.route
    ){
        composable(Screen.AddWord.route){ AddWordScreen(navController, activity = activity, user = user, userWordsViewModel) }
        composable(Screen.Tests.route){ TestsSelectionScreen(navController,activity, user = user,userWordsViewModel) }
        composable(Screen.Library.route){ LibraryScreen(navController, activity = activity, user = user, userWordsViewModel) }
        composable(Screen.SpellingTest.route){ SpellingTestScreen(navController, activity = activity, user = user) }
        composable(Screen.MultipleChoiceTest.route){ MultipleChoiceTestScreen(navController, user = user) }
        composable(Screen.HangManTest.route){ HangmanTestScreen(navController, activity = activity, user = user) }
        composable(Screen.PairingTest.route){ PairingTestScreen(navController, user = user) }
        composable(Screen.GameEnd.route){ GameEndScreen(navController,  user = user) }
    }
}

/**
 * Creates a user and builds the navigation graph.
 * The user's languages are taken from the firstLang and secondLang arguments, and the user's words are
 * retrieved from the userWordsViewModel.
 *
 * @param firstLang the first language for the user
 * @param secondLang the second language for the user
 * @param userWordsViewModel the ViewModel for the user's words
 * @param activity the MainActivity instance
 */
@Composable
fun CreateUserPassToBuildNav(firstLang: String, secondLang: String, userWordsViewModel: UserWordsViewModel = viewModel(), activity: MainActivity){

    //get the words from DB
    val wordsList by userWordsViewModel.wordsList.observeAsState(listOf())
    //create a user with languages from file and wordlist from DB
    val user = User(firstLang,secondLang,wordsList)
    //buildNav
    BuildNavigationGraph(activity = activity, user = user, userWordsViewModel)
}

@Composable
fun CreateUserDataBaseTestSetup(firstLang: String, secondLang: String, userWordsViewModel: UserWordsViewModel = viewModel(), activity: MainActivity , wordList: List<UserWord>){

    LaunchedEffect(key1 = Unit){
        wordList.forEach{ word ->
            userWordsViewModel.addWord(word)
        }


    }
    val wordsList by userWordsViewModel.wordsList.observeAsState(listOf())
    //get the words from DB

    //create a user with languages from file and wordlist from DB
    val user = User(firstLang,secondLang,wordsList)
    createLanguageFile(activity,user)
    //buildNav
    BuildNavigationGraph(activity = activity, user = user, userWordsViewModel)
}
