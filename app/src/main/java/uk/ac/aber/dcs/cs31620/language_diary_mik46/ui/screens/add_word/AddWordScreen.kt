package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.add_word

import androidx.activity.ComponentActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import uk.ac.aber.dcs.cs31620.language_diary_mik46.MainActivity
import uk.ac.aber.dcs.cs31620.language_diary_mik46.R
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.ButtonBoldText
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.MainScaffold
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.OutlinedTextFieldParams
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.textFieldParameters
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.User
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AddWordScreen(navController: NavHostController, activity: ComponentActivity, user: User, userWordsViewModel: UserWordsViewModel = viewModel()){

    val coroutineScope = rememberCoroutineScope()

    MainScaffold(
        activity,
        navController = navController,
        userWordsViewModel,
        coroutineScope = coroutineScope,
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize()
        ) {
            AddWordScreenContents(activity = activity, user = user, userWordsViewModel,coroutineScope)
        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@Composable
fun AddWordScreenContents(activity: ComponentActivity, user: User, userWordsViewModel: UserWordsViewModel = viewModel(), coroutineScope: CoroutineScope) {


    val firstTextValue = rememberSaveable { mutableStateOf("") }
    val secondTextValue = rememberSaveable { mutableStateOf("") }
    val updatedFirstWord = rememberSaveable { mutableStateOf("") }
    val updatedSecondWord = rememberSaveable { mutableStateOf("") }
    val buttonState = rememberSaveable { mutableStateOf(false) }
    val wordsList by userWordsViewModel.wordsList.observeAsState(listOf())




    Column(
        verticalArrangement = Arrangement.Center,
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        //wave image at top of screen
        Image(
            modifier = Modifier.fillMaxWidth().height(350.dp),
            painter = painterResource(R.drawable.wave_sidetoside),
            contentDescription = stringResource(R.string.wave_across),
            contentScale = ContentScale.FillBounds
        )


        OutlinedTextFieldParams(
            textValue = firstTextValue.value,
            modifier = Modifier.height(65.dp),
            onValueChange = {
                textFieldParameters(it, firstTextValue, activity)
                updatedFirstWord.value = firstTextValue.value
                //changing the button to enabled/disabled when both text fields are not empty and there are not too many spaces in a row
                buttonState.value =
                    (updatedFirstWord.value.isNotEmpty() && updatedSecondWord.value.isNotEmpty()) && !(updatedFirstWord.value.contains("  ") && updatedSecondWord.value.contains("  "))
            },
            startingText = user.firstLanguage
        )

        Spacer(modifier = Modifier.height(10.dp))

        OutlinedTextFieldParams(
            textValue = secondTextValue.value,
            modifier = Modifier.height(65.dp),
            onValueChange = {
                textFieldParameters(it, secondTextValue, activity)
                updatedSecondWord.value = secondTextValue.value
                //changing the button to enabled/disabled when both text fields are not empty and there are not too many spaces in a row
                buttonState.value =
                    (updatedFirstWord.value.isNotEmpty() && updatedSecondWord.value.isNotEmpty()) && !(updatedFirstWord.value.contains("  ") && updatedSecondWord.value.contains("  "))
            },
            startingText = user.secondLanguage


        )

        Spacer(modifier = Modifier.height(20.dp))
        ButtonBoldText(
            enabled = buttonState.value,
            onClick = {
                //using a coroutine here as this loop could be computationally intensive with higher number of words
                coroutineScope.launch {
                    val usersWord =
                        UserWord(updatedFirstWord.value, updatedSecondWord.value, false)
                    var containsWord = false
                    //getting the entire wordlist from the database
                    userWordsViewModel.wordsList.value?.forEach { word ->
                        //compare every word in the database against the new word the user is trying to enter
                        if (usersWord.firstLanguageWord.lowercase() == word.firstLanguageWord.lowercase()) {
                            containsWord = true
                        }
                        if (usersWord.secondLanguageWord.lowercase() == word.secondLanguageWord.lowercase()) {
                            containsWord = true
                        }
                    }
                    //if the word is not in database add it to the db and reset the UI
                    if (!containsWord) {
                        userWordsViewModel.addWord(usersWord)
                        user.words = wordsList
                        updatedFirstWord.value = ""
                        firstTextValue.value = ""
                        updatedSecondWord.value = ""
                        secondTextValue.value = ""
                        buttonState.value = false
                    }
                }
            },
            modifier = Modifier.fillMaxWidth().height(100.dp),
            text = stringResource(R.string.add_word)
        )
        Spacer(modifier = Modifier.fillMaxWidth().height(100.dp))
    }
}









