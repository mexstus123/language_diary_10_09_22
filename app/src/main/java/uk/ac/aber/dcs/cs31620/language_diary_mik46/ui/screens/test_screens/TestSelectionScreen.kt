package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.test_screens

import androidx.activity.ComponentActivity
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import uk.ac.aber.dcs.cs31620.language_diary_mik46.MainActivity
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.DefaultSnackBar
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.DifficultyButtons
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.MainScaffold
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.OnClickButtonGroup
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.Difficulty
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.QuestionsNeeded
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.User
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.navigation.Screen

/**
 *A composable that creates a screen with a list of buttons for the user to select from.
 * Creates 4 buttons that, when clicked, will navigate the user to a different screen for each different type of test.
 * It also displays the difficulty buttons for user to pick difficulty of tests.
 * @param navController the navigation controller that this composable will use to navigate to different screens
 * @param activity the activity that this composable is a part of
 * @param user the user object that contains information about the user, such as their name and the list of words they know
 */
@Composable
fun TestsSelectionScreen(navController: NavHostController, activity: ComponentActivity, user: User, userWordsViewModel: UserWordsViewModel = viewModel()) {

// A coroutine scope that this composable can use to launch coroutines
    val coroutineScopeUI = rememberCoroutineScope()
// A state object that this composable can use to show snackBars
    val snackbarHostState = remember{SnackbarHostState()}
// The difficulty level that the user has selected
    val difficulty = rememberSaveable{ mutableStateOf(Difficulty.MEDIUM) }

    //creating the list of buttons
    val buttonList = remember { createButtonsList(user,coroutineScopeUI,snackbarHostState,navController) }
    MainScaffold(
        activity,
        navController = navController,
        userWordsViewModel,
        snackbarHostState = snackbarHostState,
        snackbarContent ={ data ->
            DefaultSnackBar(
                data = data,
                modifier = Modifier.padding(6.dp),
                onDismiss = {
                    navController.navigate(route = Screen.AddWord.route){
                    popUpTo(navController.graph.findStartDestination().id){
                        saveState =true
                    }
                    launchSingleTop = true
                    restoreState = true
                }})
        },
        coroutineScope = coroutineScopeUI
    ) {
        Surface(
            modifier = Modifier
                .padding(0.dp, 80.dp, 0.dp, 100.dp)
                .fillMaxSize()
        ) {

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.SpaceEvenly,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(12.dp)
            ) {

                //creating difficulty buttons
                DifficultyButtons(difficulty)
                user.difficulty = difficulty.value

                Spacer(modifier = Modifier.weight(0.08F))

                //displaying buttons
                buttonList.forEach { button ->
                    Button(
                        modifier = Modifier
                            .padding(15.dp)
                            .weight(0.2F)
                            .fillMaxSize(),
                        onClick = button.onClick,
                        shape = RoundedCornerShape(20.dp),
                    ){
                        Row(
                            horizontalArrangement = Arrangement.Start,
                            verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier
                            .weight(0.5F)
                            .fillMaxSize()
                    ) {

                        Icon(
                            imageVector = button.icon,
                            contentDescription = button.buttonText,
                            modifier = Modifier
                                .size(60.dp)
                        )
                        Spacer(Modifier.size(30.dp))
                        Text(
                            button.buttonText,
                            textAlign = TextAlign.End,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 20.sp
                        )
                    }

                    }
                }
            }
        }
    }
}

/**
 * This is a helper function that creates a list of buttons for the test selection screen.
 * Each button corresponds to a different test and has an onClick listener to navigate to the corresponding test screen.
 * If the user does not have enough words in their list for a given test, a snackbar will be displayed
 * with an option to navigate to the add word screen.
 *
 * @param user a User object that contains information about the current user
 * @param coroutineScopeUI a CoroutineScope to be used for launching coroutines
 * @param difficulty the selected difficulty level for the tests
 * @param snackbarHostState a SnackbarHostState to show snackbars
 * @param navController a NavHostController to handle navigation between screens
 *
 * @return a list of OnClickButtonGroup objects representing the buttons for each test
 */
private fun createButtonsList(user: User, coroutineScopeUI: CoroutineScope, snackbarHostState: SnackbarHostState, navController: NavHostController): List<OnClickButtonGroup>{

    return listOf(
        OnClickButtonGroup(
            Icons.Default.PlayCircle,
            "Spelling",
            onClick = {
                coroutineScopeUI.launch {
                    user.createCappedRandomizedList((QuestionsNeeded.SPELLING.standard * user.difficulty.questionMultiplier).toInt())
                    if (user.randomizedQuestions.isEmpty()){
                        snackbarHostState.showSnackbar(
                            message = "${(QuestionsNeeded.SPELLING.standard * user.difficulty.questionMultiplier).toInt()} words are needed to play",
                            actionLabel = "Add Word",
                        )
                    }else{
                        navController.navigate(route = Screen.SpellingTest.route)
                    }
                }


            }
        ),OnClickButtonGroup(
            Icons.Default.Help,
            "Multiple Choice",
            onClick = {
                coroutineScopeUI.launch {
                    user.createUnCappedRandomList()
                    if (user.randomizedQuestions.isEmpty()){
                        snackbarHostState.showSnackbar(
                            message = "${(QuestionsNeeded.MULTIPLE.standard).toInt()} words are needed to play",
                            actionLabel = "Add Word",
                        )
                    }else{
                        navController.navigate(route = Screen.MultipleChoiceTest.route)
                    }
                }
            }
        ),OnClickButtonGroup(
            Icons.Default.Man,
            "Hangman",
            onClick = {
                coroutineScopeUI.launch {
                    user.setLivesToDifficulty()
                    user.createCappedRandomizedList(QuestionsNeeded.HANGMAN.standard)
                    if (user.randomizedQuestions.isEmpty()){
                        snackbarHostState.showSnackbar(
                            message = "${QuestionsNeeded.HANGMAN.standard} word is needed to play",
                            actionLabel = "Add Word",
                        )
                    }else{
                        navController.navigate(route = Screen.HangManTest.route)
                    }
                }
            }
        ),OnClickButtonGroup(
            Icons.Default.MoveDown,
            "Pairing",
            onClick = {
                coroutineScopeUI.launch {
                    user.setLivesToDifficulty()
                    user.createUnCappedRandomList()
                    if (user.randomizedQuestions.isEmpty()){
                        snackbarHostState.showSnackbar(
                            message = "${QuestionsNeeded.PAIRING.standard} words are needed to play",
                            actionLabel = "Add Word",
                        )
                    }else{
                        navController.navigate(route = Screen.PairingTest.route)
                    }
                }
            }
        )
    )
}