package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import androidx.activity.ComponentActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.launch
import uk.ac.aber.dcs.cs31620.language_diary_mik46.MainActivity
import uk.ac.aber.dcs.cs31620.language_diary_mik46.file_manager.deleteLanguageFile
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.*

/**
 * This function creates the main page navigation drawer for the app.
 * It includes a list of options the user can choose from and displays a message when an option is selected.
 * @param activity the main activity of the app
 * @param navController the navigation controller of the app
 * @param drawerState the state of the navigation drawer
 * @param userWordsViewModel the view model that holds the user's saved words
 * @param content the content of the main page
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainPageNavigationDrawer(
    activity: ComponentActivity,
    navController: NavHostController,
    drawerState: DrawerState,
    userWordsViewModel: UserWordsViewModel = viewModel(),
    content: @Composable () -> Unit = {}
){
    // selectedItem is a mutable state that is saved across composition rebuilds - it is initialized to 0
    val selectedItem = rememberSaveable{ mutableStateOf(0)}
    // showDialog is a mutable state that is saved across composition rebuilds - it is initialized to false
    val showDialog = rememberSaveable{mutableStateOf(false)}
    // coroutineScope is a [CoroutineScope] that is saved across composition rebuilds
    val coroutineScope = rememberCoroutineScope()


    // items is a list of [OnClickButtonGroup] objects representing the buttons that will be displayed in the drawer
    val items = listOf(
        OnClickButtonGroup(
            Icons.Default.Login,
            stringResource(R.string.change_language),
            message = "Changing language will delete the content of our library.\n Do you still wish to change language?",
            onClick = {
                // launch a coroutine in the saved coroutine scope
                coroutineScope.launch {
                    // close the drawer
                    drawerState.close()
                    // delete all data from the database
                    userWordsViewModel.deleteAllFromDatabase()
                    // delete the language file
                    deleteLanguageFile(activity)
                    // recreate the activity
                    activity.recreate()
                }}
        ),
        OnClickButtonGroup(
            Icons.Default.Help,
            stringResource(R.string.help),
            message = "Use the \"Add Word\" section to add words to your library.\n Once you have enough words try out some of the test in the \"Tests\" section.\nHave fun learning!",
            onClick = {}
        ),
        OnClickButtonGroup(
            Icons.Default.Feedback,
            stringResource(R.string.feedback),
            message = "Any feedback is much appreciated.\n star rating?",
            onClick = {}
        ),
        OnClickButtonGroup(
            Icons.Default.ExitToApp,
            stringResource(R.string.exit),
            message = "This will close the app.\n Do you still wish to close the leave?",
            onClick = {activity.finish()}
        )
    )

    ModalNavigationDrawer(
        drawerState = drawerState,
        drawerContent = {
            if (showDialog.value) {
                AlertDialog(

                    title = { Text(text = items[selectedItem.value].buttonText) },
                    text = {
                        Text(
                            text = items[selectedItem.value].message,
                            textAlign = TextAlign.Center
                        )
                    },
                    onDismissRequest = { showDialog.value = false},
                    icon = {
                        Icon(
                            imageVector = items[selectedItem.value].icon,
                            contentDescription = items[selectedItem.value].buttonText
                        )
                    },
                    dismissButton = {
                        Button(
                            onClick = { showDialog.value = false }) {
                            Text(stringResource(R.string.close))
                        }
                    },
                    confirmButton = {
                        when(selectedItem.value) {
                            0 -> {
                                Button(
                                    onClick = {
                                    items[selectedItem.value].onClick.invoke()
                                    showDialog.value = false
                                    }
                                ) {
                                    Text("Confirm")
                                }
                            }
                            3 -> {
                                Button(
                                    onClick = {
                                    items[selectedItem.value].onClick.invoke()
                                    showDialog.value = false
                                    }
                                ) {
                                    Text("Confirm")
                                }
                            }
                            else -> { }
                        }
                    }

                )
            }


        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier

                .fillMaxSize()
        ) {
            Text(text = "Menu",
            modifier = Modifier
                .padding(12.dp))
            items.forEachIndexed { index, item ->
                NavigationDrawerItem(
                    icon = {
                        Icon(
                            imageVector = item.icon,
                            contentDescription = item.buttonText
                        )
                    },
                    label = { Text(item.buttonText)},
                    selected = index == selectedItem.value,
                    onClick = {
                        selectedItem.value = index
                        showDialog.value = true
                    }
                )
            }
        }
    },
    //page content
    content = content
    )
}