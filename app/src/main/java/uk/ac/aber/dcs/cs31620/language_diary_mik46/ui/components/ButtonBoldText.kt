package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

/**
 * Composable function that creates a button with bold text.
 *
 * @param modifier the [Modifier] for the button
 * @param onClick lambda to be called when the button is clicked
 * @param text the text to be displayed on the button
 * @param enabled whether the button is enabled or not. Default is `true`.
 */
@Composable
fun ButtonBoldText(
    modifier: Modifier = Modifier,
    onClick: () -> Unit = {},
    text: String,
    enabled: Boolean = true
) {
    Button(
        elevation = ButtonDefaults.buttonElevation(defaultElevation = 6.dp),
        onClick = onClick,
        enabled = enabled,
        modifier = modifier
            .fillMaxWidth()
            .padding(20.dp)
    ) {
        Text(text = text,
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp
        )
    }
}