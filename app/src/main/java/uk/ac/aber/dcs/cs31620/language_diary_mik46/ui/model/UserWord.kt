package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * A data class representing a word pair with a first language and second language translation.
 * The first language word is also the primary key for this data class.
 *
 * @param firstLanguageWord The first language word in this pair.
 * @param secondLanguageWord The second language translation of the first language word.
 * @param isFavourite A boolean indicating whether this word pair is marked as a favorite.
 */
@Entity(tableName = "UsersWords")
data class UserWord(

    @PrimaryKey
    var firstLanguageWord: String = "",
    var secondLanguageWord: String = "",
    var isFavourite: Boolean = false
)
