package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

/**
 * A composable function that displays a list of 4 choice buttons
 * for the multiple choice game type. It allows the user to select one of answers before guessing.
 *
 * @param modifier the [Modifier] to be applied to the outermost [Column]
 * @param buttonNumber a [MutableState] holding the index of the selected button. If the value is not in the range 0 to 3, no button will be selected.
 * @param unselectedColour the [Color] to use for unselected buttons
 * @param selectedColour the [Color] to use for the selected button
 * @param wordList a list of 4 strings representing the labels for the buttons
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MultipleChoiceButtons(modifier: Modifier,buttonNumber: MutableState<Int>, unselectedColour: Color, selectedColour: Color, wordList: List<String>) {


    // Create a list of 4 colors, all initialized to the unselected color
    val buttonColors = MutableList(4) { unselectedColour }

    // If the value in buttonNumber is in the range 0 to 3, set the corresponding element in buttonColors to the selected color
    if (buttonNumber.value in 0..3) {
        buttonColors[buttonNumber.value] = selectedColour
    }

    // The main layout for the multiple choice buttons, using a vertical [Column]
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        // Iterate over the list of buttons in pairs (two buttons per row)
        for (i in 0 until 4 step 2) {
            // A horizontal [Row] to hold two buttons
            Row(
                modifier = Modifier.fillMaxWidth().weight(0.5F),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                // The first button in the pair
                Surface(
                    modifier = Modifier
                        .weight(0.5F)
                        .fillMaxSize()
                        .padding(15.dp),
                    shadowElevation = 6.dp,
                    color = buttonColors[i],  // Use the corresponding color from buttonColors
                    shape = RoundedCornerShape(10.dp),
                    onClick = {
                        // Update buttonNumber when the button is clicked
                        buttonNumber.value = i
                    }
                ) {
                    Column(
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            modifier = Modifier,
                            text = wordList[i],  // Use the corresponding label from wordList
                            fontWeight = FontWeight.Bold
                        )
                    }
                }
                // The second button in the pair
                if (i + 1 < 4) {
                    Surface(
                        modifier = Modifier
                            .weight(0.5F)
                            .fillMaxSize()
                            .padding(15.dp),
                        shadowElevation = 6.dp,
                        color = buttonColors[i + 1],
                        shape = RoundedCornerShape(10.dp),
                        onClick = {
                            buttonNumber.value = i + 1
                        }) {
                        Column(
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Text(
                                modifier = Modifier,
                                text = wordList[i + 1],
                                fontWeight = FontWeight.Bold
                            )
                        }
                    }
                }
            }
        }
    }
}