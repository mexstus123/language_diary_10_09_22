package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers

import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import java.security.SecureRandom
import java.util.concurrent.TimeUnit

/**
 * Base class for managing a game using a list of words.
 *
 * @param words The list of words to use in the game.
 * @param startTime The start time of the game in milliseconds.
 */
open class GameManager(val words: List<UserWord>, private val startTime: Long = System.currentTimeMillis()) {

    /**
     * A secure random number generator.
     */
    val secureRandom = SecureRandom()

    /**
     * Gets a random word pair from the list of words.
     *
     * @return A random word pair.
     */
    fun pickRandomWordPair(): UserWord {
        return words[secureRandom.nextInt(words.size)]
    }

    /**
     * Determines whether the first language or second language of the word pair should be displayed first.
     *
     * @return `true` if the first language should be displayed first, `false` if the second language should be displayed first.
     */
    fun randomizeIsFirstLanguage(): Boolean{
        return secureRandom.nextBoolean()
    }

    /**
     * Gets a random word from the list of words.
     *
     * @return A random word.
     */
    fun pickRandomWord(): String{
        if (randomizeIsFirstLanguage()) {
            return pickRandomWordPair().firstLanguageWord
        }
        return pickRandomWordPair().secondLanguageWord
    }

    /**
     * Gets the elapsed time since the start of the game.
     *
     * @return The elapsed time in the format "MM:SS".
     */
    fun getElapsedTime(): String{
        val elapsedTime = System.currentTimeMillis() - startTime
        val minutes = TimeUnit.MILLISECONDS.toMinutes(elapsedTime)
        val seconds = TimeUnit.MILLISECONDS.toSeconds(elapsedTime) - TimeUnit.MINUTES.toSeconds(minutes)

        return String.format("%02d:%02d", minutes, seconds)
    }

    /**
     * Gets the last question number for the game.
     * Hangman and pairing game types are a constant number of questions so it is open to allow polymorphism
     * @param difficulty The difficulty level of the game.
     * @return The last question number.
     */
    open fun getLastQuestionNumber(difficulty: Difficulty): Int {
        return when (difficulty) {
            Difficulty.EASY -> {
                4
            }
            Difficulty.MEDIUM -> {
                8
            }
            Difficulty.HARD -> {
                12
            }
        }
    }
}