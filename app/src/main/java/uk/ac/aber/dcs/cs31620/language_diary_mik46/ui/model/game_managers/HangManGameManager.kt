package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers


import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord

/**
 * Manages the hangman game using a list of words.
 *
 * @param words The list of words to use in the game - passes them to super.
 */
class HangManGameManager(words: List<UserWord>): GameManager(words) {


    /**
     * The user's correct guesses as a string.
     * Making sure spaces are always included
     */
    private var correctUserGuesses = " "


    /**
     * Gets a blanked version of a random word
     * using regex to replace all chars except spaces.
     *
     * @return A pair of the original word and the blanked version.
     */
    fun createBlankedWord(): Pair<String,String>{
        val selectedWord = pickRandomWord()
        return Pair(selectedWord,selectedWord.replace(Regex("[^\\s]"), "_"))
        }

    /**
     * Determines if the user's guess is correct.
     *
     * @param userGuess The user's guess.
     * @param word The original word.
     * @return `true` if the guess is correct, `false` otherwise.
     */
    fun guessedCorrectly(userGuess: Char,word: String): Boolean{
        word.forEach { char->
            if (char.equals(userGuess, ignoreCase = true)){
                correctUserGuesses += userGuess
                return true
            }
        }
        return false
    }

    /**
     * Gets an updated version of the blanked word with correctly guessed letters filled in.
     *
     * @param currentWord The current pair of the original word and the blanked version.
     * @return The updated blanked word.
     */
    fun updatedBlankedWord(currentWord: Pair<String,String>): String{
        //if no user guesses so far the word is fully blanked
        if (correctUserGuesses == ""){
            return currentWord.second
        }
        val blankedTemp = currentWord.first
        val replacement = '_'
        val guesses = correctUserGuesses.toCharArray()

        //Using regex to blank out all except the user guesses - includes space
        return blankedTemp.lowercase().replace(Regex("[^${guesses.joinToString("").lowercase()}$replacement]"), replacement.toString())
    }

    /**
     * Gets last question number.
     * used for ending the game and the endGame screen
     *
     * @param difficulty difficulty of game - not used in this case but needed for polymorphism.
     * @return last question number for hang man game type.
     */
    override fun getLastQuestionNumber(difficulty: Difficulty): Int {
        return 1
        }
}