package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import uk.ac.aber.dcs.cs31620.language_diary_mik46.R

/**
 * MainPageTopAppBar is a composable that displays a top app bar.
 * @param onClick: a lambda representing the action that should be taken when the navigation icon is clicked, default value is an empty lambda
 */
@Preview
@Composable
fun MainPageTopAppBar(
    onClick: () -> Unit = {}
){
    // the CenterAlignedTopAppBar composable displays the top app bar
    CenterAlignedTopAppBar(
        // specify the colors for the top app bar
        colors = TopAppBarDefaults.mediumTopAppBarColors(containerColor = MaterialTheme.colorScheme.primary),
        // the title of the top app bar
        title = {
            // display the app title text
            Text(stringResource(R.string.app_title), color = MaterialTheme.colorScheme.onPrimary)
        },
        // the navigation icon for the top app bar
        navigationIcon = {
            // the IconButton displays the navigation icon and calls the onClick lambda when clicked
            IconButton(onClick = onClick) {
                // display the menu icon
                Icon(
                    imageVector = Icons.Filled.Menu,
                    contentDescription = stringResource(androidx.compose.ui.R.string.navigation_menu),
                    // specify the tint color for the icon
                    tint = MaterialTheme.colorScheme.onPrimary
                )
            }
        }
    )
}



