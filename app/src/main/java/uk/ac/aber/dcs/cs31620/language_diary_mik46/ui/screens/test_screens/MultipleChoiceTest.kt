package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.test_screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.*
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.User
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.MultipleChoiceGameManager
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.navigation.Screen
import uk.ac.aber.dcs.cs31620.language_diary_mik46.R

/**
 * This is a composable function that displays a multiple choice test screen for a given user.
 * It allows the user to select a choice and displays whether they got the question correct or not.
 * It also keeps track of the number of correct answers and the elapsed time.
 * When the user reaches the final question, it navigates to the game end screen.
 *
 * @param navController a NavHostController to handle navigation between screens
 * @param user a User object that contains information about the current user
 */
@Composable
fun MultipleChoiceTestScreen(navController: NavHostController, user: User) {


    val gameManager = remember {  MultipleChoiceGameManager(user.randomizedQuestions) }
    val questionNumber = remember{ mutableStateOf(1) }
    val correctAnswers = remember { mutableStateOf(0) }
    val wordList = remember { mutableStateOf(gameManager.getChoiceWordList()) }

    val currentButtonSelected = remember { mutableStateOf(-1) }
    val colourSelector = remember { mutableStateOf(0) }

    TestsScaffold(
        navController = navController
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(0.26F),
                painter = painterResource(id = R.drawable.wave_tests),
                contentDescription = stringResource(R.string.wavey_test_image),
                contentScale = ContentScale.FillBounds
            )
            Surface(
                modifier = Modifier
                    .weight(0.2F)
                    .fillMaxSize()
                    .padding(20.dp),
                color = FadeAnimationColours(colourSelector.value),
                shape = RoundedCornerShape(20.dp)
            ) {
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = wordList.value.second.firstLanguageWord,
                        color = MaterialTheme.colorScheme.secondary,
                        fontSize = 40.sp
                    )
                }

            }


            Spacer(modifier = Modifier.weight(0.04F))
            MultipleChoiceButtons(
                modifier = Modifier.weight(0.2F),
                currentButtonSelected,
                MaterialTheme.colorScheme.primaryContainer,
                MaterialTheme.colorScheme.primary,
                wordList.value.first
            )

            ButtonBoldText(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(0.13F),
                text = stringResource(R.string.next_question),
                onClick = {
                    //if a button has been selected
                    if(currentButtonSelected.value != -1) {
                        //coroutine for animations
                        CoroutineScope(Dispatchers.Main).launch {
                            //if the user has guessed correctly
                            if (wordList.value.second.secondLanguageWord.lowercase() == wordList.value.first[currentButtonSelected.value]) {
                                //play animations and update correct answers
                                animatePrimaryContainerColour(true, colourSelector)
                                correctAnswers.value += 1
                            } else {
                                //if users guess was incorrect play incorrect animations
                                animatePrimaryContainerColour(false, colourSelector)
                            }
                            //deselect button
                            currentButtonSelected.value = -1
                            //check for game end
                            if (questionNumber.value == gameManager.getLastQuestionNumber(user.difficulty)) {
                                //if it was the last question update user variables
                                user.questionTotal = gameManager.getLastQuestionNumber(user.difficulty)
                                user.answersCorrect = correctAnswers.value
                                user.elapsedTime = gameManager.getElapsedTime()
                                //navigate to end screen
                                navController.navigate(Screen.GameEnd.route)
                            } else {
                                //if its not last question go to next question
                                questionNumber.value += 1
                                wordList.value = gameManager.getChoiceWordList()
                            }
                        }
                    }
                }
            )
            Spacer(modifier = Modifier.weight(0.1F))
        }

    }
}

