package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.TweenSpec
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.ui.graphics.Color
import kotlinx.coroutines.delay

/**
 * Animate color of the object as a state.
 * The color transitions between its current value and the target value over a given duration.
 *
 * @param targetValue the color value to animate to
 * @return the current animated color value
 */
@Composable
fun FadeAnimationColours(colourSelector: Int): Color {
    val animation by animateColorAsState(
        targetValue =
        when (colourSelector) {
            0 -> {
                MaterialTheme.colorScheme.primaryContainer
            }
            1 -> {
                MaterialTheme.colorScheme.error
            }
            2 -> {
                MaterialTheme.colorScheme.errorContainer
            }
            3 -> {
                MaterialTheme.colorScheme.background
            }
            4 -> {
                MaterialTheme.colorScheme.primary
            }
            else -> {MaterialTheme.colorScheme.primaryContainer}
        },
        animationSpec = TweenSpec<Color>(durationMillis = 200, easing = LinearEasing)
    )
    return animation
}

/**
 *This is the animation for each games container to show if user got the answer correct
 *
 * @param isCorrect whether the answer is correct
 * @param colourSelector the current color of the object
 */
suspend fun animatePrimaryContainerColour(isCorrect: Boolean, colourSelector: MutableState<Int>){
    //Changes the color of an object to the errorContainer color if the answer is correct,

    if (isCorrect){
        colourSelector.value = 2
        delay(200)
        colourSelector.value = 0

    // or the error container color if the answer is incorrect,
    }else{
        colourSelector.value = 1
        delay(200)
        colourSelector.value = 0
    }
}
/**
 * This is the animation used for pairing buttons
 *
 * @param isCorrect whether the answer is correct
 * @param colourSelectorButtonOne the current color of button 1
 * @param colourSelectorButtonTwo the current color of button 2
 */
suspend fun animateButtonPair(isCorrect: Boolean, colourSelectorButtonOne: MutableState<Int>, colourSelectorButtonTwo: MutableState<Int>){
    //Changes the color of an object to the errorContainer color and then to background colour if the answer is correct
    if (isCorrect){
        colourSelectorButtonOne.value = 2
        colourSelectorButtonTwo.value = 2
        delay(250)
        colourSelectorButtonOne.value = 3
        colourSelectorButtonTwo.value = 3

    }else{
        // or the error container color and goes back to unselected colour if the answer is incorrect,
        colourSelectorButtonOne.value = 1
        colourSelectorButtonTwo.value = 1
        delay(250)
        colourSelectorButtonOne.value = 0
        colourSelectorButtonTwo.value = 0
    }
}



