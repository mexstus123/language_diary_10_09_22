package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.screens.test_screens

import androidx.activity.ComponentActivity
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uk.ac.aber.dcs.cs31620.language_diary_mik46.MainActivity
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components.*
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.HangManGameManager
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.User
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWordsViewModel
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.navigation.Screen
import uk.ac.aber.dcs.cs31620.language_diary_mik46.R

/**
 * This function creates the UI for the Hangman Test screen and allows the user to play a game of Hangman
 * using their primary and secondary language vocabulary.
 *
 * @param navController the NavHostController that is used to navigate between screens
 * @param activity the MainActivity instance that this composable function is being called from
 * @param user the User object that contains information about the user's language choices and progress
 */
@Composable
fun HangmanTestScreen(navController: NavHostController, activity: ComponentActivity, user: User) {


    val gameManager = remember { HangManGameManager(user.randomizedQuestions)}
    var userGuess = '\\'
    val usersTextFieldGuess = remember { mutableStateOf("") }
    val usersLives = remember { mutableStateOf(user.lives) }
    val currentWordPair = remember { mutableStateOf(gameManager.createBlankedWord()) }
    val blankedWord = remember { mutableStateOf(currentWordPair.value.second) }
    val colourSelector = remember { mutableStateOf(0) }


    TestsScaffold(
        navController = navController
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp),
                painter = painterResource(id = R.drawable.wave_tests),
                contentDescription = stringResource(R.string.wavey_test_image),
                contentScale = ContentScale.FillBounds
            )
            Surface(
                modifier = Modifier
                    .height(190.dp)
                    .fillMaxWidth()
                    .padding(20.dp),
                color = FadeAnimationColours(colourSelector.value),
                shape = RoundedCornerShape(20.dp)
            ) {
                Column(verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
                    Text(
                        text = blankedWord.value,
                        color = MaterialTheme.colorScheme.secondary,
                        fontSize = 40.sp)//TODO add list of randomized words
                }

            }

            Surface(modifier = Modifier
                .height(100.dp)
                .width(100.dp)) {

                Image(
                    modifier = Modifier
                        .fillMaxSize()
                        ,
                    painter = painterResource(id = R.drawable.heart),
                    contentDescription = stringResource(R.string.heart),
                    contentScale = ContentScale.FillBounds
                )
                Text(text = usersLives.value.toString(), textAlign = TextAlign.Center, fontSize = 40.sp, modifier = Modifier.padding(20.dp), fontWeight = FontWeight.Bold, color = MaterialTheme.colorScheme.secondary)
            }
            Spacer(modifier = Modifier.height(10.dp))
            Text(text = stringResource(id = R.string.enter_a_letter), textAlign = TextAlign.Center, fontSize = 12.sp)
            Spacer(modifier = Modifier.height(1.dp))
            Surface(modifier = Modifier
                .height(60.dp)
                .width(50.dp),
                shape = RoundedCornerShape(10.dp),
                border = BorderStroke(1.dp, MaterialTheme.colorScheme.secondary)
            ) {
                TextFieldParams(
                    modifier = Modifier.fillMaxSize(),
                    textValue = usersTextFieldGuess.value,
                    onValueChange = {
                        textFieldParameters(it, usersTextFieldGuess, activity, 1)
                    },
                    label = "",
                )
            }


            ButtonBoldText(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(100.dp),
                text = stringResource(R.string.guess),
            onClick = {
                // coroutine for animations and other computational things
                CoroutineScope(Dispatchers.Main).launch {
                    //making sure there is a user input
                    if (usersTextFieldGuess.value.isNotEmpty()) {
                        //saving users guess and emptying the text field contents
                        userGuess = usersTextFieldGuess.value.first()
                        usersTextFieldGuess.value = ""

                        //if the guess is correct play correct animation and update the blanked word
                        if (gameManager.guessedCorrectly(userGuess,currentWordPair.value.first)) {
                            animatePrimaryContainerColour(true,colourSelector)
                            blankedWord.value = gameManager.updatedBlankedWord(currentWordPair.value)
                        } else {
                            //if user guess is incorrect update lives, play incorrect animation
                            user.lives -= 1
                            usersLives.value -= 1
                            animatePrimaryContainerColour(false,colourSelector)
                            //check for game lost
                            if (user.lives == 0) {
                                //sending game end screen information to user class
                                user.questionTotal = gameManager.getLastQuestionNumber(user.difficulty)
                                user.elapsedTime = gameManager.getElapsedTime()
                                // go to end screen
                                navController.navigate(Screen.GameEnd.route)
                            }
                        }
                        //check for game win
                        if (!blankedWord.value.contains('_')) {
                            //sending game end screen information to user clas
                            user.questionTotal = gameManager.getLastQuestionNumber(user.difficulty)
                            user.elapsedTime = gameManager.getElapsedTime()
                            user.answersCorrect += 1
                            // go to end screen
                            navController.navigate(Screen.GameEnd.route)
                        }
                    }
                }
            }
            )
        }

    }
}


