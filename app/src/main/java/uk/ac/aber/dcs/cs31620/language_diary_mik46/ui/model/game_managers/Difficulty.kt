package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers


/**
 * Represents the difficulty level of a game.
 *
 * @param questionMultiplier The multiplier applied to the number of questions.
 * @param livesMultiplier The multiplier applied to the number of lives.
 */
enum class Difficulty(val questionMultiplier: Double, val livesMultiplier: Double ) {
    EASY(0.5,1.5),
    MEDIUM(1.0,1.0),
    HARD(1.5,0.5)
}
