package uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.outlined.*
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.currentBackStackEntryAsState
import uk.ac.aber.dcs.cs31620.language_diary_mik46.R
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.navigation.Screen
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.navigation.mainScreens

/**
 * Composable function that creates a bottom navigation bar with filled/outlined icons and different colours for each screen.
 *
 * @param navController the [NavController] to navigate between screens
 */
@Composable
fun MainPageNavigationBar(
    navController: NavController
){
    // Map each screen to an IconGroup object containing the icons and label for the screen
    val icons = mapOf (
        Screen.AddWord to IconGroup(
            filledIcon = Icons.Filled.Mode,
            outlineIcon = Icons.Outlined.Mode,
            label = stringResource(id = R.string.add_word)
        ),
        Screen.Tests to IconGroup(
            filledIcon = Icons.Filled.CheckCircle,
            outlineIcon = Icons.Outlined.CheckCircle,
            label = stringResource(id = R.string.tests)
        ),
        Screen.Library to IconGroup(
            filledIcon = Icons.Filled.Article,
            outlineIcon = Icons.Outlined.Article,
            label = stringResource(id = R.string.library)
        )

    )

    NavigationBar {
        // Get the current back stack entry for the nav controller
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        // Get the destination for the current back stack entry
        val currentDestination = navBackStackEntry?.destination

        // For each screen, create a NavigationBarItem with the appropriate icon and label
        mainScreens.forEach { screen ->
            // Determine if the screen is selected based on its route being in the current destination's hierarchy
            val isSelected = currentDestination
                ?.hierarchy?.any { it.route == screen.route } == true
            // Get the label text for the screen
            val labelText = icons[screen]!!.label
            NavigationBarItem(
                // Set the icon for the screen
                icon = {
                    Icon(
                        imageVector = (
                                if (isSelected)
                                    icons[screen]!!.filledIcon
                                else
                                    icons[screen]!!.outlineIcon),
                        contentDescription = labelText
                    )
                },
                // Set the label for the screen
                label = { Text(labelText) },
                // Set the selected state for the screen
                selected = isSelected,
                // Set the onClick listener for the screen
                onClick = {
                    navController.navigate(screen.route) {
                        // Pop up to the start destination and save the state
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        // Set the launch mode to singleTop
                        launchSingleTop = true
                        // Restore the state when navigating
                        restoreState = true
                    }
                }
            )
        }
    }
}