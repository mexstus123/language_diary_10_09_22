package uk.ac.aber.dcs.cs31620.language_diary_mik46.game_manager_tests

import junit.framework.TestCase.assertEquals
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.Difficulty
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.HangManGameManager

class HangManGameManagerTests {

    private val gameManager = HangManGameManager(listOf(UserWord("word1", "word2")))

    @Test
    fun testCreateBlankedWord() {
        val blankedWord = gameManager.createBlankedWord()
        assertEquals("word1", blankedWord.first)
        assertEquals("_____", blankedWord.second)
    }

    @Test
    fun testGuessedCorrectly_correctGuess() {
        val result = gameManager.guessedCorrectly('w', "word1")
        assertEquals(true, result)
    }

    @Test
    fun testGuessedCorrectly_incorrectGuess() {
        val result = gameManager.guessedCorrectly('x', "word1")
        assertEquals(false, result)
    }

    @Test
    fun testUpdatedBlankedWord_noGuesses() {
        val currentWord = gameManager.createBlankedWord()
        val updatedWord = gameManager.updatedBlankedWord(currentWord)
        assertEquals("_____", updatedWord)
    }

    @Test
    fun updatedBlankedWord_fillsInCorrectGuesses() {
        gameManager.guessedCorrectly('d', "word1")
        assertEquals("___d_", gameManager.updatedBlankedWord(Pair("word1","_____")))
        gameManager.guessedCorrectly('1', "word1")
        assertEquals("___d1", gameManager.updatedBlankedWord(Pair("word1","_____")))
    }

    @Test
    fun updatedBlankedWord_ignoresCapitalization() {
        gameManager.guessedCorrectly('W', "word1")
        assertEquals("w____", gameManager.updatedBlankedWord(Pair("word1","_____")))
    }

    @Test
    fun getLastQuestionNumber_returnsCorrectNumber() {
        assertEquals(1, gameManager.getLastQuestionNumber(Difficulty.EASY))
        assertEquals(1, gameManager.getLastQuestionNumber(Difficulty.MEDIUM))
        assertEquals(1, gameManager.getLastQuestionNumber(Difficulty.HARD))
    }
}