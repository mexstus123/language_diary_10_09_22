package uk.ac.aber.dcs.cs31620.language_diary_mik46.game_manager_tests

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.MultipleChoiceGameManager

class MultipleChoiceGameManagerTests {

    private val gameManager = MultipleChoiceGameManager(List<UserWord>(9){index ->UserWord("dog${index}", "perro${index}")})


    @Test
    fun getChoiceWordList_sizeIs4AndContainsCorrectAnswer() {
        val choiceList = gameManager.getChoiceWordList()
        assertEquals(4, choiceList.first.size)
        assertTrue(choiceList.first.contains(choiceList.second.secondLanguageWord))
    }
    @Test
    fun getNoneIdenticalWord_noIdenticalWords() {
        for (i in 0..10) {
            val choiceList = gameManager.getChoiceWordList()
            choiceList.first.forEachIndexed {index,word ->
                assertTrue(choiceList.first.lastIndexOf(word) == index)
            }

        }
    }

}