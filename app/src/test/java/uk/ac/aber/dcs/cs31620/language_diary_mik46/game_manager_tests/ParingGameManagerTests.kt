package uk.ac.aber.dcs.cs31620.language_diary_mik46.game_manager_tests

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.PairingGameManager

class ParingGameManagerTests {

    private val userWord1 = UserWord("word1", "word2")
    private val userWord2 = UserWord("word3", "word4")
    private val userWord3 = UserWord("word5", "word6")
    private val userWord4 = UserWord("word7", "word8")
    private val userWord5 = UserWord("word9", "word10")
    private val words = listOf(userWord1, userWord2, userWord3, userWord4, userWord5)
    private val gameManager = PairingGameManager(words)

    @Test
    fun testGetShuffledWordList() {
        val (allWordsList, userWordList) = gameManager.getShuffledWordList()
        assertEquals(10, allWordsList.size)
        assertTrue(allWordsList.contains(userWord1.firstLanguageWord))
        assertTrue(allWordsList.contains(userWord1.secondLanguageWord))
        assertTrue(allWordsList.contains(userWord2.firstLanguageWord))
        assertTrue(allWordsList.contains(userWord2.secondLanguageWord))
        assertTrue(allWordsList.contains(userWord3.firstLanguageWord))
        assertTrue(allWordsList.contains(userWord3.secondLanguageWord))
        assertTrue(allWordsList.contains(userWord4.firstLanguageWord))
        assertTrue(allWordsList.contains(userWord4.secondLanguageWord))
        assertTrue(allWordsList.contains(userWord5.firstLanguageWord))
        assertTrue(allWordsList.contains(userWord5.secondLanguageWord))
        assertEquals(5, userWordList.size)
        assertTrue(userWordList.contains(userWord1))
        assertTrue(userWordList.contains(userWord2))
        assertTrue(userWordList.contains(userWord3))
        assertTrue(userWordList.contains(userWord4))
        assertTrue(userWordList.contains(userWord5))

    }
    //NOTE - THIS TEST IS RANDOM AND CAN FAIL
    @Test
    fun getShuffledWordList_RandomnessTest_FAILABLE() {
        assertTrue(gameManager.getShuffledWordList().first != gameManager.getShuffledWordList().first)
    }
}
