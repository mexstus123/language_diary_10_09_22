package uk.ac.aber.dcs.cs31620.language_diary_mik46

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.User
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.Difficulty

class UserTests {
    private val user = User(
        "English",
        "Spanish",
        List<UserWord>(9){index ->UserWord("hello$index", "hola$index")}
    )

    @Test
    fun createCappedRandomizedListCorrectSizeTest() {
        user.createCappedRandomizedList(9)
        assertEquals(9, user.randomizedQuestions.size)
    }
    @Test
    fun createCappedRandomizedListSizeLargerThanListTest() {
        user.createCappedRandomizedList(10)
        assertEquals(0, user.randomizedQuestions.size)
    }
    @Test
    fun createCappedRandomizedListContentsSameAfterRandomize() {
        user.words = listOf(
            UserWord("hello", "hola"),
            UserWord("goodbye", "adios"),
            UserWord("dog", "perro")
        )
        user.createCappedRandomizedList(3)
        assertEquals(true, user.randomizedQuestions.contains(UserWord("hello", "hola")))
        assertEquals(true, user.randomizedQuestions.contains(UserWord("goodbye", "adios")))
        assertEquals(true, user.randomizedQuestions.contains(UserWord("dog", "perro")))
    }

    @Test
    fun createUnCappedRandomList5SizeMinimumTest() {
        user.words = List<UserWord>(3){index ->UserWord("hello$index", "hola$index")}
        user.createUnCappedRandomList()
        assertEquals(0, user.randomizedQuestions.size)
    }

    @Test
    fun createUnCappedRandomListSizeLimitTest() {
        user.words = List<UserWord>(35){index ->UserWord("hello$index", "hola$index")}
        user.createUnCappedRandomList()
        assertEquals(30, user.randomizedQuestions.size)
    }

    @Test
    fun setRandomizedLivesChangedCorrectLivesTest() {
        user.difficulty = Difficulty.HARD
        user.setLivesToDifficulty()
        assertEquals(2, user.lives)

        user.difficulty = Difficulty.MEDIUM
        user.setLivesToDifficulty()
        assertEquals(5, user.lives)

        user.difficulty = Difficulty.EASY
        user.setLivesToDifficulty()
        assertEquals(7, user.lives)
    }

}
