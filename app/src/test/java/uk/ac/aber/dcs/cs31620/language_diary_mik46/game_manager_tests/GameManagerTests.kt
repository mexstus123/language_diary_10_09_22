package uk.ac.aber.dcs.cs31620.language_diary_mik46.game_manager_tests

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.Before
import org.junit.Test
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.UserWord
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.Difficulty
import uk.ac.aber.dcs.cs31620.language_diary_mik46.ui.model.game_managers.GameManager

class GameManagerTests {

    private lateinit var gameManager: GameManager
    private val words = listOf(
        UserWord("word1", "word1"),
        UserWord("word2", "word2"),
        UserWord("word3", "word3")
    )

    @Before
    fun setUp() {
        gameManager = GameManager(words)
    }

    @Test
    fun `getElapsedTime should return elapsed time in MM SS format`() {
        // sleep for 1 second to create an elapsed time
        Thread.sleep(1000)
        val elapsedTime = gameManager.getElapsedTime()
        assertTrue(elapsedTime.matches(Regex("\\d\\d:\\d\\d")))
    }

    @Test
    fun `getLastQuestionNumber should return last question number for game`() {
        val lastQuestionNumberEasy = gameManager.getLastQuestionNumber(Difficulty.EASY)
        assertEquals(4, lastQuestionNumberEasy)

        val lastQuestionNumberMedium = gameManager.getLastQuestionNumber(Difficulty.MEDIUM)
        assertEquals(8, lastQuestionNumberMedium)

        val lastQuestionNumberHard = gameManager.getLastQuestionNumber(Difficulty.HARD)
        assertEquals(12, lastQuestionNumberHard)
    }
}


